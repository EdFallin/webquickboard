// /**/
//
// import {DomList} from "../domlist.js";
// import {DomItem} from "../domitem.js";
// import {Origin} from "../origin.js";
//
// // region Fixtures
//
// function supplyRealisticBuilderAndExtractor() /* verified */ {
//     // Realistic builder.
//     let builder = (item, id) => {
//         let root = document.createElement("div");
//         root.classList.add("root");
//         root.id = id;
//
//         let leaf = document.createElement("span");
//         leaf.id = "Content";
//         leaf.innerText = item;
//
//         root.appendChild(leaf);
//         return root;
//     };
//
//     // Realistic extractor.
//     let extractor = (el) => {
//         // Traversing parent nodes rootward to the 
//         // DomItem's in-DOM root, where its ID is.
//         while (!el.classList.contains("root")) {
//             el = el.parentNode;
//         }
//
//         // Back to caller.
//         return el.id;
//     };
//
//     return {builder, extractor};
// }
//
// function supplyEmptyBuilderAndExtractor() {
//     // These are as empty as is possible.
//     let builder = (item, id) => {
//         return null;
//     };
//     let extractor = (el) => {
//         return 0;
//     };
//
//     return {builder, extractor};
// }
//
// function supplyArgsForNewDomList(builder, extractor) /* verified */ {
//     // Args for DomList constructor besides items.
//     let root = document.createElement("div");
//     let origin = new Origin(builder, extractor);
//
//     return {root, origin};
// }
//
// // endregion Fixtures
//
// describe("DomList", () => {
//     // region constructor()
//
//     it("constructor() throws when its origin arg is not an Origin instance.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let root = document.createElement("div");
//         let origin = "origin";
//
//         let expected = DomList.notAnOrigin;
//         let actual = "Failed.";
//
//         /* Act. */
//         try {
//             let instance = new DomList(items, root, origin);
//         }
//         catch (x) {
//             actual = x.message;
//         }
//
//         /* Assert. */
//         expect(actual).to.equal(expected);
//     });
//
//     it("constructor() retains all args directly except items.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let root = document.createElement("div");
//         let origin = new Origin("builder", "extractor");
//
//         /* Act. */
//         let instance = new DomList(items, root, origin);
//
//         /* Assert. */
//         expect(instance.root).to.equal(root);
//         expect(instance.origin).to.equal(origin);
//     });
//
//     it("constructor() has no DomItems in .domItems when its items arg is empty.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//         let origin = new Origin(emptyBuilder, "extractor");
//
//         /* Act. */
//         let instance = new DomList(items, root, origin);
//
//         /* Assert. */
//         expect(instance.domItems.length).to.equal(0);
//     });
//
//     it("constructor() inits a DomItem in .domItems for each item in items arg.", /* working */ () => {
//         /* Arrange. */
//         let items = ["one", "two", "three"];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//         let origin = new Origin(emptyBuilder, "extractor");
//
//         /* Act. */
//         let instance = new DomList(items, root, origin);
//
//         /* Assert. */
//         // All were inited.
//         expect(instance.domItems.length).to.equal(items.length);
//
//         // All are DomItems.
//         for (let actual of instance.domItems) {
//             expect(actual).to.equalInstanceOf(DomItem);
//         }
//     });
//
//     // endregion constructor()
//
//     // region domItemOfEl()
//
//     it("domItemOfEl() returns the DomItem in .domItems matching the el arg.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let root = document.createElement("div");
//
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let expected = instance.domItems[2];
//         let argEl = document.createElement("div");
//         argEl.id = expected.id;
//
//         /* Act. */
//         let actual = instance.domItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equal(expected);
//     });
//
//     it("domItemOfEl() returns null if no DomItem in .domItems matches the el arg.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.domItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equalNull();
//     });
//
//     it("domItemOfEl() returns null if .origin.extractor throws.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             throw new Error("Extractor threw.");
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.domItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equalNull();
//     });
//
//     // endregion domItemOfEl()
//
//     // region domItemIndexOfEl()
//
//     it("domItemIndexOfEl() returns the index of the element in .domItems matching the el arg.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let expected = instance.domItems[2];
//         let argEl = document.createElement("div");
//         argEl.id = expected.id;
//
//         /* Act. */
//         let actual = instance.domItemIndexOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equal(2);
//     });
//
//     it("domItemIndexOfEl() returns DomList.notPresent if no DomItem in .domItems matches the el arg.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.domItemIndexOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equal(DomList.notPresent);
//     });
//
//     it("domItemIndexOfEl() returns DomList.notPresent if .origin.extractor throws.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             throw new Error("Extractor threw.");
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.domItemIndexOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.equal(DomList.notPresent);
//     });
//
//     // endregion domItemOfEl()
//
//     // region doesContainDomItemOfEl()
//
//     it("doesContainDomItemOfEl() returns true if the el is in one of .domItems.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let expected = instance.domItems[2];
//         let argEl = document.createElement("div");
//         argEl.id = expected.id;
//
//         /* Act. */
//         let actual = instance.doesContainDomItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.be.true;
//     });
//
//     it("doesContainDomItemOfEl() returns false if the el is not in any of .domItems.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3]
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             return el.id;
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.doesContainDomItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.be.false;
//     });
//
//     it("doesContainDomItemOfEl() returns false if .origin.extractor throws.", /* working */ () => {
//         /* Arrange. */
//         let items = [1, 2, 3]
//         let root = document.createElement("div");
//         let emptyBuilder = (item, id) => {
//         };
//
//         let extractor = (el) => {
//             throw new Error("Extractor threw.");
//         };
//
//         let origin = new Origin(emptyBuilder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let argEl = document.createElement("div");
//         argEl.id = String(7);
//
//         /* Act. */
//         let actual = instance.doesContainDomItemOfEl(argEl);
//
//         /* Assert. */
//         expect(actual).to.be.false;
//     });
//
//     // endregion doesContainDomItemOfEl()
//
//     // region addDomItem()
//
//     it("addDomItem() adds the DomItem before the existing DomItem for the el provided.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//         let toAdd = new DomItem("New", root, 7, builder, extractor, null);
//
//         // Arg el from an element of .domItems.
//         let atDomItem = instance.domItems[3];
//         let atEl = atDomItem.node.querySelector("#Content");
//
//         let expecteds = ["A", "b", "C", "New", "d"];
//
//         /* Act. */
//         instance.addDomItem(toAdd, atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("addDomItem() adds the DomItem at the start if the arg el is for the first .domItems element.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//         let toAdd = new DomItem("Added", root, 7, builder, extractor, null);
//
//         // Arg el from an element of .domItems.
//         let atDomItem = instance.domItems[0];
//         let atEl = atDomItem.node.querySelector("#Content");
//
//         let expecteds = ["Added", "A", "b", "C", "d"];
//
//         /* Act. */
//         instance.addDomItem(toAdd, atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("addDomItem() adds the DomItem at the end if the el is not part of .domItems.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//         let toAdd = new DomItem("New", root, 7, builder, extractor, null);
//
//         // Arg el that's not from a member of .domItems.
//         let atEl = document.createElement("span");
//
//         let expecteds = ["A", "b", "C", "d", "New"];
//
//         /* Act. */
//         instance.addDomItem(toAdd, atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("addDomItem() adds the DomItem as the sole item when .domItems is empty.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//         let toAdd = new DomItem("Only", root, 7, builder, extractor, null);
//
//         // Arg el that's not from a member of .domItems.
//         let atEl = document.createElement("span");
//
//         let expecteds = ["Only"];
//
//         /* Act. */
//         instance.addDomItem(toAdd, atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     // endregion addDomItem()
//
//     // region subtractDomItem()
//
//     it("subtractDomItem() removes the DomItem from .domItems for the arg el.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[2];
//         let atEl = toSubtract.node.querySelector("#Content");
//
//         let expecteds = ["A", "b", "d"];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("subtractDomItem() calls subtract() on the DomItem it is about to remove.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[2];
//         let atEl = toSubtract.node.querySelector("#Content");
//
//         let expected = "Subtractor was called on C.";
//         let actual;
//
//         // Test result.
//         for (let domItem of instance.domItems) {
//             domItem.subtractor = () => {
//                 // Can't use .this here, since here it's calling context.
//                 actual = `Subtractor was called on ${domItem.item}.`;
//             };
//         }
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actual).to.equal(expected);
//     });
//
//     it("subtractDomItem() doesn't do anything if the arg el is not part of a .domItems element.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el based on an element of .domItems, but with a different .id.
//         let originalDomItem = instance.domItems[2];
//         let copiedNode = originalDomItem.node.cloneNode(true);
//         copiedNode.id = 200;
//         let atEl = copiedNode.querySelector("#Content");
//
//         let expecteds = ["A", "b", "C", "d"];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("subtractDomItem() removes a first element in .domItems correctly.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[0];
//         let atEl = toSubtract.node.querySelector("#Content");
//
//         let expecteds = ["b", "C", "d"];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("subtractDomItem() removes a sole item correctly.", /* working */ () => {
//         /* Arrange. */
//         let items = ["N"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[0];
//         let atEl = toSubtract.node.querySelector("#Content");
//
//         let expecteds = [];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("subtractDomItem() removes the last item correctly.", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[3];
//         let atEl = toSubtract.node.querySelector("#Content");
//
//         let expecteds = ["A", "b", "C"];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("subtractDomItem() doesn't do anything if the list is empty.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         // Arg el from an element of .domItems.
//         let toSubtract = instance.domItems[3];
//         let atEl = document.createElement("span");
//         atEl.innerText = "g";
//
//         let expecteds = [];
//
//         /* Act. */
//         instance.subtractDomItem(atEl);
//
//         /* Gather results. */
//         let actuals = instance.domItems.map(x => x.item);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     // endregion subtractDomItem()
//
//     // region display()
//
//     it("display() calls clearDisplayedDomItems() and then displayEachDomItem().", /* working */ () => {
//         /* Arrange. */
//         let items = ["A", "b", "C", "d"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         // Target object and arg.
//         let instance = new DomList(items, root, origin);
//
//         let counter = 0;
//
//         // Spoofed methods that keep track of their calling order, 
//         // if they're called, using nonce properties on DomList.
//         instance.clearDisplayedDomItems = () => {
//             instance.countAtClearDisplayed = counter++;
//         };
//
//         instance.displayEachDomItem = () => {
//             instance.countAtDisplayEach = counter++;
//         };
//
//         /* Act. */
//         instance.display();
//
//         /* Assert. */
//         expect(instance.countAtClearDisplayed).to.equal(0);
//         expect(instance.countAtDisplayEach).to.equal(1);
//     });
//
//     // endregion display()
//
//     // region clearDisplayedDomItems()
//
//     it("clearDisplayedDomItems() removes all child nodes of .root", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let {builder, extractor} = supplyEmptyBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         // Test condition: child nodes to.equal removed.
//         instance.root.appendChild(document.createElement("div"));
//         instance.root.appendChild(document.createElement("span"));
//         instance.root.appendChild(document.createElement("div"));
//
//         /* Act. */
//         instance.clearDisplayedDomItems();
//
//         /* Assert. */
//         expect(instance.root.children.length).to.equal(0);
//     });
//
//     it("clearDisplayedDomItems() does nothing when .root has no child nodes.", /* working */ () => {
//         /* Arrange. */
//         let items = [];
//         let {builder, extractor} = supplyEmptyBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         /* Pre-testing. */
//         // Test condition: .root has no nodes.
//         expect(instance.root.children.length).to.equal(0);
//
//         /* Act. */
//         instance.clearDisplayedDomItems();
//
//         /* Assert. */
//         expect(instance.root.children.length).to.equal(0);
//     });
//
//     // endregion clearDisplayedDomItems()
//
//     // region displayEachDomItem()
//
//     it("displayEachDomItem() calls display() on each DomItem in .domItems in order.", /* working */ () => {
//         /* Arrange. */
//         let items = ["1", "2", "3"];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let expecteds = ["Item 1", "Item 2", "Item 3"];
//         let actuals = [];
//
//         // Test condition: calling redefined.
//         for (let at = 0; at < items.length; at++) {
//             let domItem = instance.domItems[at];
//
//             domItem.display = () => {
//                 actuals.push(`Item ${domItem.item}`);
//             };
//         }
//
//         /* Act. */
//         instance.displayEachDomItem();
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("displayEachDomItem() does nothing when .domItems is empty.", /* working */ () => {
//         /* Arrange. */
//         // Test condition: no items arg, so no .domItems.
//         let items = [];
//         let {builder, extractor} = supplyRealisticBuilderAndExtractor();
//         let {root, origin} = supplyArgsForNewDomList(builder, extractor);
//
//         let instance = new DomList(items, root, origin);
//
//         let expecteds = [];
//         let actuals = [];
//
//         // Code that runs if there are elements in .domItems.
//         for (let at = 0; at < items.length; at++) {
//             let domItem = instance.domItems[at];
//
//             domItem.display = () => {
//                 actuals.push(domItem.item);
//             };
//         }
//
//         /* Act. */
//         instance.displayEachDomItem();
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     // endregion displayEachDomItem()
//
//     /* Cruft : future: Drop these if never used. */
//     // it("getItem() returns the item at each given index.", /* working */ () => {
//     // it("getItem() returns null if the index is out of range.", /* working */ () => {
//     //
//     // it("getItemIndex() returns the correct .items index for each in-list item.", /* working */ () => {
//     // it("getItemIndex() returns DomList.notPresent for any item that isn't in its list.", /* working */ () => {
//     //
//     // it("doesContainItem() returns true for each in-list item.", /* working */ () => {
//     // it("doesContainItem() returns false for any item that isn't in its list.", /* working */ () => {
//     //
//     // it("addItem() adds the provided item at the list index for the at-item provided.", /* working */ () => {
//     // it("addItem() invokes displayList() after the list is updated.", /* working */ () => {
//     //
//     // it("subtractItem() removes the provided item from the list.", /* working */ () => {
//     // it("subtractItem() invokes displayList() after the list is updated.", /* working */ () => {
//     //
//     // it("createNodeItemFromStencil() adds a new item at the end.", /* working */ () => {
//     // it("createNodeItemFromStencil() provides the instance's .start to its .starter", /* working */ () => {
//     // it("createNodeItemFromStencil() invokes displayList() after the list is updated.", /* working */ () => {
// });
//
