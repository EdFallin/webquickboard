// /**/
//
// import {DomItem} from "../domitem.js";
//
// // region Fixtures
//
// function supplyInitingArgs() {
//     // Trivial basic args.
//     let item = {value: "item"};
//     let site = {value: "site"};
//     let id = 3;
//
//     // Delegate args.
//     let builder = (item, id) => {
//         return {value: item.value, id: id };
//     }
//     let subtractor = () => { };
//
//     // Back to caller.
//     return {item, site, id, builder, subtractor };
// }
//
// // endregion Fixtures
//
// describe("DomItem", () => {
//     // region constructor()
//
//     it("constructor() sets arg properties to args provided.", /* working */ () => {
//         /* Arrange. */
//         let {item, site, id, builder, subtractor } = supplyInitingArgs();
//
//         /* Act. */
//         let target = new DomItem(item, site, id, builder, subtractor);
//
//         /* Assert. */
//         // Equating identities, or for id, values.
//         expect(target.item).to.equal(item);
//         expect(target.site).to.equal(site);
//         expect(target.id).to.equal(id);
//         expect(target.builder).to.equal(builder);
//         expect(target.subtractor).to.equal(subtractor);
//     });
//
//     it("constructor() calls .builder with right args.", /* working */ () => {
//         /* Arrange. */
//         let {item, site, id, builder } = supplyInitingArgs();
//
//         let expected = {value: "item", id: id };
//
//         /* Act. */
//         let target = new DomItem(item, site, id, builder);
//
//         /* Assert. */
//         expect(target.node).to.equal(expected);
//     });
//
//     // endregion constructor()
//
//     // region equals()
//
//     it("equals() returns false when the .id's of the two DomItems are different.", /* working */ () => {
//         /* Arrange. */
//         let {item, site, id, builder } = supplyInitingArgs();
//
//         // The id args are different.
//         let target = new DomItem(item, site, id, builder );
//         let untarget = new DomItem(item, site, id + 1, builder );
//
//         /* Act. */
//         let actual = target.equals(untarget);
//
//         /* Assert. */
//         expect(actual).to.be.false;
//     });
//
//     it("equals() returns true when the .id's of the two DomItems are the same.", /* working */ () => {
//         /* Arrange. */
//         let {item, site, id, builder } = supplyInitingArgs();
//
//         // The same instance also has the same id;
//         // the only case in which they should match.
//         let target = new DomItem(item, site, id, builder );
//         let untarget = target;
//
//         /* Act. */
//         let actual = target.equals(untarget);
//
//         /* Assert. */
//         expect(actual).to.be.true;
//     });
//
//     // endregion equals()
//
//     // region display()
//
//     it("display() adds .node to the children of .site", /* working */ () => {
//         /* Arrange. */
//         let {item, id} = supplyInitingArgs();
//
//         let site = document.createElement("div");
//         let builder = (item, id) => {
//             let node = document.createElement("span");
//             node.id = id;
//             return node;
//         };
//
//         let instance = new DomItem(item, site, id, builder);
//
//         /* Act. */
//         instance.display();
//
//         /* Assert. */
//         expect(instance.site.children.length).to.equal(1);
//         expect(instance.site.children[0]).to.equal(instance.node);
//     });
//
//     // endregion display()
//
//     // region subtract()
//    
//     it("subtract() calls any .subtractor present.", /* working */ () => {
//         /* Arrange. */
//         let { item, site, id, builder } = supplyInitingArgs();
//        
//         let expected = `Subtracting ${ id }`;
//         let actual = `Failed.`;
//        
//         let subtractor = () => { actual = expected; };
//        
//         let target = new DomItem(item, site, id, builder, subtractor);
//        
//         /* Act. */
//         target.subtract();
//        
//         /* Assert. */
//         expect(actual).to.equal(expected);
//     });
//    
//     it("subtract() does not throw when there is no .subtractor.", /* working */ () => {
//         /* Arrange. */
//         let { item, site, id, builder } = supplyInitingArgs();
//
//         let subtractor = null;
//
//         let target = new DomItem(item, site, id, builder, subtractor);
//
//         let actual = null;
//        
//         /* Act. */
//         try {
//             target.subtract();
//         }
//         catch (thrown) {
//             actual = thrown;
//         }
//
//         /* Assert. */
//         expect(actual).to.equalNull();
//     });
//    
//     // endregion subtract()
// });
