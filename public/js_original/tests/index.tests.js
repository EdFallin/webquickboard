// /**/
//
// import {IndexController} from "../index.js";
// import {BlurbSet} from "../blurbset.js";
// import { Blurb } from "../blurb.js";
// import { DomList } from "../domlist.js";
// import { DomItem } from "../domitem.js";
//
// // region Fixtures
//
// function supplyDataToSpoofStoring() {
//     let keys = ["a", "b", "c"];
//     let items = [
//         {key: "a", item: new BlurbSet("Third", [], 2)},
//         {key: "b", item: new BlurbSet("First", [], 0)},
//         {key: "c", item: new BlurbSet("Second", [], 1)}
//     ];
//
//     let jsonItems = items.map(x => JSON.stringify(x.item));
//     return {keys, items, jsonItems};
// }
//
// function redefineInstanceStorageToSpoofs(instance, keys, items, jsonItems) {
//     instance.storage.length = keys.length;
//
//     instance.storage.key = (at) => {
//         return keys[at];
//     };
//
//     instance.storage.getItem = (key) => {
//         let at = items.findIndex(x => x.key === key);
//         return jsonItems[at];
//     }
// }
//
// // endregion Fixtures
//
// describe("IndexController", () => {
//     it("retrieveAnyBlurbContent() instantiates BlurbSets for retrieved JSON.", /* working */ () => {
//         /* Arrange. */
//         let instance = new IndexController(document);
//
//         // Input for test condition.
//         let {keys, items, jsonItems} = supplyDataToSpoofStoring();
//         redefineInstanceStorageToSpoofs(instance, keys, items, jsonItems);
//
//         let expecteds = ["First", "Second", "Third"];
//
//         /* Act. */
//         instance.retrieveAnyBlurbContent();
//
//         /* Gather results. */
//         let actuals = instance.blurbSets;
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals.length).to.equal(instance.storage.length);
//         for (let actual of actuals) {
//             expect(actual).to.equalInstanceOf(BlurbSet);
//         }
//     });
//
//     it("retrieveAnyBlurbContent() sorts the retrieved BlurbSets by each one's .index.", /* working */ () => {
//         /* Arrange. */
//         let instance = new IndexController(document);
//
//         // Input for test condition.
//         let { keys, items, jsonItems } = supplyDataToSpoofStoring();
//         redefineInstanceStorageToSpoofs(instance, keys, items, jsonItems);
//
//         let expecteds = ["First", "Second", "Third"];
//
//         /* Act. */
//         instance.retrieveAnyBlurbContent();
//
//         /* Gather results. */
//         let actuals = instance.blurbSets.map(x => x.name);
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals).to.equal(expecteds);
//     });
//
//     it("retrieveAnyBlurbContent() doesn't do anything when there are no stored BlurbSets.", /* working */ () => {
//         /* Arrange. */
//         let keys = [];
//         let items = [];
//
//         let instance = new IndexController(document);
//
//         /* Act. */
//         instance.retrieveAnyBlurbContent();
//
//         /* Gather results. */
//         let actuals = instance.blurbSets;
//
//         /* Assert. */
//         // Array equivalence.
//         expect(actuals.length).to.equal(0);
//     });
//
//     it("convertBlurbSetsToDomList() adds each existing BlurbSet as a DomItem in a DomList.", /* working */ () => {
//         /* Arrange. */
//         let instance = new IndexController(document);
//        
//         instance.setOrigin.builder = (item, id) => {
//             let root = document.createElement("div");
//             root.id = id;
//             root.innerText = item;
//         };
//        
//         instance.setOrigin.extractor = (el) => {
//             return el.id;
//         };
//        
//         let sets = [
//             new BlurbSet("One", [], 3, false),
//             new BlurbSet("Two", [], 4, true)
//         ];
//        
//         sets[0].blurbs.push(new Blurb("A", Blurb.plain, 100, 100, 0));
//         sets[1].blurbs.push(new Blurb("B", Blurb.plain, 200, 200, 0));
//         sets[1].blurbs.push(new Blurb("C", Blurb.plain, 300, 300, 1));
//        
//         instance.blurbSets = sets;
//        
//         let expecteds = [
//             { name: "One", blurbs: ["A"] },
//             { name: "Two", blurbs: ["B", "C"] }
//         ];
//        
//         /* Act. */
//         instance.convertBlurbSetsToDomList();
//        
//         /* Gather results. */
//         let raw = instance.blurbSetDomList.domItems;
//         let actuals = raw.map(domItem => {
//             let blurbSet = domItem.item;
//
//             let blurbs = blurbSet.blurbs.map(blurb => blurb.text);
//             let result = { name: blurbSet.name, blurbs };
//             return result;
//         });
//
//         /* Assert. */
//         expect(actuals).to.equal(expecteds);
//     });
// });
