// /**/
//
// import { BlurbSet } from "../blurbset.js";
//
// describe("BlurbSet", () => {
//     it("fromJson() returns an object with the values in the JSON.", /* working */ () => {
//         /* Arrange. */
//         let plain = {
//             name: "Set Name",
//             blurbs: ["a", "b", "c", "d"]
//         };
//
//         let json = JSON.stringify(plain);
//
//         /* Act. */
//         let actual = BlurbSet.fromJson(json);
//
//         /* Assert. */
//         expect(actual.name).to.equal(plain.name);
//         expect(actual.blurbs).to.equal(plain.blurbs);
//     });
//
//     it("fromJson() returns an object with optional properties set to JSON values when they are in the JSON.", /* working */ () => {
//         /* Arrange. */
//         let plain = {
//             text: "Name",
//             blurbs: [],
//             index: 6,
//             isFocused: false
//         };
//        
//         let json = JSON.stringify(plain);
//        
//         /* Act. */
//         let actual = BlurbSet.fromJson(json);
//        
//         /* Assert. */
//         expect(actual.index).to.equal(6);
//         expect(actual.isFocused).to.be.false;
//     });
//    
//     it("fromJson() returns an object with defaults for optional properties when they aren't in the JSON.", /* working */ () => {
//         /* Arrange. */
//         let plain = {
//             text: "Name",
//             blurbs: []
//         };
//
//         let json = JSON.stringify(plain);
//
//         /* Act. */
//         let actual = BlurbSet.fromJson(json);
//
//         /* Assert. */
//         expect(actual.index).to.equal(0);
//         expect(actual.isFocused).to.be.true;
//     });
// });
