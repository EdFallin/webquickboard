/**/

export class DomItem {
    constructor(item, site, id, builder, subtractor) /* passed */ {
        // Content, location, and identity.
        this.item = item;
        this.site = site;
        this.id = id;
        
        this.subtractor = subtractor;

        // DOM-element creator and creation.
        this.builder = builder;
        this.node = this.builder(this.item, this.id);
    }

    display() /* passed */ {
        // Display of the el already created.
        this.site.appendChild(this.node);
    }

    equals(other) /* passed */ {
        return this.id === other.id;
    }

    subtract() /* passed */ {
        if (!this.subtractor) {
            return;
        }
        
        this.subtractor();
    }
}
