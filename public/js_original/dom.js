/**/

export class Dom {
    constructor(doc) {
        this.doc = doc;

        this.groupElStencil = doc.getElementById("GroupElStencil");
        this.groupBlurbsElStencil = doc.getElementById("GroupBlurbsElStencil");
        this.blurbElStencil = doc.getElementById("BlurbElStencil");

        this.emptyBlurbGroupElStencil = doc.getElementById("EmptyBlurbGroupElStencil");
        this.emptyBlurbElStencil = doc.getElementById("EmptyBlurbElStencil");

        this.blurbSets = doc.getElementById("BlurbSets");
        this.blurbs = doc.getElementById("Blurbs");
    }
}
