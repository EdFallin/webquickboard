/**/

import {DomItem} from "./domitem.js";
import {Origin} from "./origin.js";

export class DomList {
    /* Supports adding and subtracting existing items, and adding
       all-new ones as well.  Doesn't handle persistent storage. */

    static get notAnOrigin() {
        return "DomList's origin argument must be an instance of Origin.";
    }

    static get notPresent() {
        return -1;
    }

    // 'Items' is an array of items for the DomList to display.
    // 'Root' is a node in the DOM in which the list will be displayed.
    // 'Origin' is a readied instance of the Origin class. 
    constructor(items, root, origin) /* passed */ {
        if (!(origin instanceof Origin)) {
            throw new Error(DomList.notAnOrigin);
        }

        this.root = root;
        this.origin = origin;

        this.domItems = [];

        for (let item of items) {
            let domItem = new DomItem(
                item,
                this.root,
                ++this.origin.index,
                this.origin.builder
            );

            this.domItems.push(domItem);
        }
    }

    display() /* passed */ {
        this.clearDisplayedDomItems();
        this.displayEachDomItem();
    }

    clearDisplayedDomItems() /* passed */ {
        // Removing the entire existing list's DOM representation.
        while (this.root.children.length > 0) {
            this.root.removeChild(this.root.lastChild);
        }
    }

    displayEachDomItem() /* passed */ {
        for (let domItem of this.domItems) {
            domItem.display();
        }
    }

    domItemOfEl(el) /* passed */ {
        try {
            // Find DomItem by extracted ID, if it's present.
            let id = this.origin.extractor(el);
            let domItem = this.domItems.find(x => x.id == id);

            // It often may not be present.
            if (!domItem) {
                domItem = null;
            }

            // Back to caller.
            return domItem;
        }
        catch {
            // If anything goes wrong, treat it as not present.
            return null;
        }
    }

    domItemIndexOfEl(el) /* passed */ {
        // Find its index by extracted ID, or else the not-present default.
        try {
            let id = this.origin.extractor(el);
            let index = this.domItems.findIndex(x => x.id == id);
            return index;
        }
        catch {
            return DomList.notPresent;
        }
    }

    doesContainDomItemOfEl(el) /* passed */ {
        let domItem = this.domItemOfEl(el);
        return domItem !== null;
    }

    addDomItem(domItem, atEl) /* passed */ {
        // Get insert point.
        let at = this.domItemIndexOfEl(atEl);

        // When no insert point, added at end.
        if (at === DomList.notPresent) {
            at = this.domItems.length;
        }

        // Actually adding by splicing in.
        this.domItems.splice(at, 0, domItem);
    }

    subtractDomItem(atEl) /* passed */ {
        // Get removal point.
        let at = this.domItemIndexOfEl(atEl);
        
        // Because splicing works with a negative index.
        if (at === DomList.notPresent) {
            return;
        }
        
        // Allowing the DomItem to do any of its subtraction steps.
        let target = this.domItems[at];
        target.subtract();
        
        // Actually subtracting.
        this.domItems.splice(at, 1);
    }

    addItem(item, atEl) {
        /* Cruft, future:  Is this needed? */
    }


    // region items

    getItem(index) /* passed */ {
        // Edge paths.
        if (index < 0 || index >= this.items.length) {
            return null;
        }

        // Main path.
        return this.domItems[index];
    }

    doesContainItem(item) /* passed */ {
        let index = this.getItemIndex(item);
        return index != DomList.notPresent;
    }

    getItemIndex(item) /* passed */ {
        let index = this.domItems.findIndex(x => this.matcher(x, item));
        return index;
    }

    // addItem(item, atItem) /* passed */ {
    //     let index = this.getItemIndex(atItem);
    //
    //     // Actually adding.
    //     this.items.splice(index, 0, item);
    //
    //     this.displayList();
    // }

    // subtractItem(item) /* passed */ {
    //     let index = this.getItemIndex(item);
    //
    //     // Actually subtracting.
    //     this.items.splice(index, 1);
    //
    //     this.displayList();
    // }

    // endregion items

}
