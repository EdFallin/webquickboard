/**/

/* Defines things needed by DomLists and DomItems,
   which may receive them through DomLists. */
export class Origin {
    // DomItemBuilder is code to create a DOM node from a 
    // source and set its root's ID to the current .index.
    // DomItemExtractor is code to work rootward from a DOM el 
    // until it finds a DomItem root, and then return its ID.
    constructor(domItemBuilder, domItemIdExtractor) {
        this.index = 0;
        this.builder = domItemBuilder;
        this.extractor = domItemIdExtractor;
    }
    
}
