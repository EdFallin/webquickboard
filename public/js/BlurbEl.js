/**/

import { DomEl } from "./DomEl.js";
import { GroupBlurbsEl } from "./GroupBlurbsEl.js";

/* The blurbs themselves! */
export class BlurbEl extends DomEl {
    get blurb() {
        return this._blurb;
    }

    get text() {
        return this._text;
    }

    get level() {
        return this._level;
    }

    get height() {
        return this._height;
    }

    get width() {
        return this._width;
    }

    /* An override. */
    get class() {
        return "BlurbEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.blurbElStencil;
    }

    constructor(blurb, parent) {
        super(parent);

        this._blurb = blurb;

        this._text = blurb.text;
        this._level = blurb.level;
        this._height = blurb.height;
        this._width = blurb.width;
    }

    _displaySelf() /* passed */ {
        let at = this.parent.children.indexOf(this);

        let listRoot = this.parent.listRoot;
        let before = listRoot.children[at];

        listRoot.insertBefore(this.domNode, before);

        this.setText();
        this.setLevelStyling();
    }

    setText() {
        let text = this._domNode.querySelector("#Text");
        text.value = this._text;
    }

    setLevelStyling() {
        this._domNode.classList.add(this._blurb.level);
    }
    
    /* An override. */
    clone() {
        let cloned = new this.constructor(this.blurb, this.parent);
        
        /* No child objects. */
        this._cloneLeafHtml(cloned);
        
        return cloned;
    }

    canDropThisOnto(domEl) /* passed */ {
        if (domEl instanceof BlurbEl) {
            return true;
        }
        
        if (domEl instanceof GroupBlurbsEl) {
            return true;
        }
        
        return false;
    }
    
    _whenDroppedOntoThis(e) {
        e.stopPropagation();
        let dragged = this._addToParent(e);
        dragged.display();
    }
    
    // region actual QuickBoard functionality (copy and paste)

    /* &cruft, work these into the object system */

    atBlurbClick(e) {
        let target = e.target;

        // If a child el with another purpose was clicked, ignore this click.
        if (!this.elHasAnyOfTheseClasses(target, "clickable", "inner-blurb")) {
            e.stopPropagation();
            return;
        }

        this.copyFromBlurbText(target);
    }

    atCopyButtonClick(e) {
        let target = e.target;
        this.copyFromBlurbText(target);
    }

    copyFromBlurbText(el) {
        let blurbEl = this.blurbElFromEl(el);
        let text = this.textFromEl(blurbEl, "Text");

        this.io.writeToClipboard(text)
            .then(() => {
                    this.flashBlurbEl(blurbEl);
                }
            );
    }

    textFromEl(rootEl, textElName) {
        /* The textEl here is assumed to be an Input
           or TextArea with a .value attribute. */

        textElName = `#${ textElName }`;
        let textEl = rootEl.querySelector(textElName);

        let text = textEl.value;
        return text;
    }

    flashBlurbEl(blurbEl) {
        let targets = blurbEl.querySelectorAll("textarea, button");
        targets = [blurbEl, ...targets];
        
        for (let target of targets) {
            target.classList.add("flash");
        }
        
        setTimeout(() => {
                for (let target of targets) {
                    target.classList.remove("flash");
                }
            }, 250
        );
    }

    // endregion actual QuickBoard functionality (copy and paste)

    /* &cruft, work into object system */
    atDumpBlurbButtonClick(e) {
        let target = e.target;

        let blurbEl = this.ancestorFromElAndClass(target, "blurb");
        let region = this.ancestorFromElAndClass(blurbEl, "region");

        // Drop from the blurb set.
        let blurbSet = this.setFromBlurbParent(region);
        let text = this.textFromEl(blurbEl, "Text");
        let blurb = blurbSet.blurbFromText(text);
        blurbSet.dumpBlurb(blurb);

        // Drop from the stored blurb set.
        blurbSet.save();

        // Drop from the tree and DOM.
        region.removeChild(blurbEl);
        blurbEl.domNode.remove();
    }
    
}
