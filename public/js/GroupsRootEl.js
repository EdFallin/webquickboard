/**/

import { DomEl } from "./DomEl.js";
import { GroupEl } from "./GroupEl.js";
import { EmptyGroupEl } from "./EmptyGroupEl.js";

/* Displays the list of group names.  
   Its child nodes are GroupEls. */
export class GroupsRootEl extends DomEl {
    /* An override. */
    get class() {
        return "GroupsRootEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.groupsRoot;
    }

    get listRoot() {
        return this._listRoot;
    }

    get emptyGroup() {
        return this._emptyGroup;
    }

    // region constructor() and dependencies

    /* The .children here are GroupEl objects. */
    constructor(parent, children) {
        super(parent, children);

        /* Additional members.  Children are displayed in 
         * one child node, and the empty group in another. */
        this._initSubtreeMembers();
    }

    _initSubtreeMembers() {
        this._listRoot = this._domNode.querySelector("#ListRoot");

        let empty = this._domNode.querySelector("#Empty");
        this._emptyGroup = new EmptyGroupEl(empty);
    }

    _initDomNode() {
        this._domNode = this.stencil;
    }

    _displaySelf() {
        /* GroupsRootEl is a permanent DOM node that is always displayed, 
         * and a final empty EmptyGroupEl for starting a new blurb group. */

        this._emptyGroup.display();
    }
    
    // endregion constructor() and dependencies

    /* An override. */
    _whenDroppedOntoThis(e) {
        let dragged = this._addAsChild(e);
        dragged.display();
    }

    /* An override. */
    _addAsChild(e) {
        // Original, or a clone.
        let dragged = this._calculateDragged(e);

        // Where to put dragged as a new child.
        let onto = this._calculateOntoChild(e);

        // Drag-onto-self makes no change.
        if (dragged === onto) {
            return;
        }

        // Removing and reindexing, if not cloned.
        this._removeDraggedFromParentIfMoving(e, dragged);

        // Actually adding as child.
        this.addChild(dragged, onto);

        // Displays the GroupEl, and its 
        // GBE if it should be displayed.
        dragged.display();

        // Needed for callers to call display().
        return dragged;
    }

    /* An override. */
    _calculateOntoChild(e) {
        let onto = null;

        for (let at = 0; at < this.listRoot.children.length; at++) {
            let domChild = this.listRoot.children[at];

            let box = domChild.getBoundingClientRect();
            let top = box.top;

            if (e.clientY < top) {
                onto = this.children[at];
                break;
            }
        }

        return onto;
    }
}
