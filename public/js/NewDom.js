/**/

export class NewDom {
    constructor(doc) /* passed */ {
        this.doc = doc;
        
        /* DOM nodes where DOM els for Composite objects are displayed. */
        this.groupsRoot = doc.getElementById("GroupsRoot");
        this.blurbsRoot = doc.getElementById("BlurbsRoot");

        /* Stencils retained in the DOM for cloning to create new display elements. */
        this.emptyGroupElStencil = doc.getElementById("EmptyGroupElStencil");
        this.groupBlurbsElStencil = doc.getElementById("GroupBlurbsElStencil");
        this.groupElStencil = doc.getElementById("GroupElStencil");

        this.blurbElStencil = doc.getElementById("BlurbElStencil");
        this.emptyBlurbElStencil = doc.getElementById("EmptyBlurbElStencil");

        /* Basis for IDs that are unique across all DomEl subclass objects, 
           1-based because incremented before supplied to consumer. */
        this._id = 0;
    }

    static get singleton() /* passed */ {
        NewDom._singleton = NewDom._singleton || new NewDom(document);
        return NewDom._singleton
    }

    static set singleton(value) /* passed */ {
        NewDom._singleton = value;
    }

    get nextId() /* passed */ {
        return ++this._id;
    }
}
