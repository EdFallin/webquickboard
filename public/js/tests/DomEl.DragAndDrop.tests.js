/**/

import { expect } from "@esm-bundle/chai";

import { DomEl } from "../DomEl.js";
import { DragAndDrop } from "../DragAndDrop.js";

import { DomElFixtures } from "./fixtures/DomEl.fixtures.js";

describe("DomEl — drag and drop", () => {
    describe("_whenThisDraggedFrom()", () => {
        it("stopPropagation() is called on its event arg.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);
            let e = DomElFixtures.createMinimalSpoofEventArg("e.target");

            /* Act. */
            target._whenThisDraggedFrom(e);

            /* Assert. */
            expect(e.stopWasCalled).to.be.true;
        });

        it("DragAndDrop's .dragged is set to the originating DomEl.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);
            let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

            /* Act. */
            target._whenThisDraggedFrom(e);

            /* Assemble. */
            let actual = target._dnd.dragged;

            /* Assert. */
            expect(actual).to.equal(target);
        });

    });

    describe("_whenOtherDraggedOverThis()", () => {
        it("preventDefault() is called on its event arg.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);
            target._dnd.dragged = new DomEl(null, []);

            let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

            /* Act. */
            target._whenOtherDraggedOverThis(e);

            /* Assert. */
            expect(e.defaultWasPrevented).to.be.true;
        });

        describe("dragged.canDropThisOnto() is false.", () => {
            it("The event arg's dataTransfer.dropEffect is set to `none`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return false;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");
                e.dataTransfer.dropEffect = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.dropEffect).to.equal("none");
            });

            it("The event arg's dataTransfer.effectAllowed is set to `none`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return false;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");
                e.dataTransfer.effectAllowed = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.effectAllowed).to.equal("none");
            });

            it("stopPropagation() is not called on its event arg.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return false;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");
                e.stopWasCalled = false;

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.stopWasCalled).to.be.false;
            });

        });

        describe("dragged.canDropThisOnto() is true.", () => {
            it("When neither shift nor alt / option is pressed, the event arg's dataTransfer.dropEffect is set to `move`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = false;
                e.altKey = false;
                e.dataTransfer.dropEffect = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.dropEffect).to.equal("move");
            });

            it("When neither shift nor alt / option is pressed, the event arg's dataTransfer.effectAllowed is set to `move`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = false;
                e.altKey = false;
                e.dataTransfer.effectAllowed = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.effectAllowed).to.equal("move");
            });

            it("When shift is pressed, the event arg's dataTransfer.dropEffect is set to `copy`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = true;
                e.altKey = false;
                e.dataTransfer.dropEffect = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.dropEffect).to.equal("copy");
            });

            it("When shift is pressed, the event arg's dataTransfer.effectAllowed is set to `copy`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = true;
                e.altKey = false;
                e.dataTransfer.effectAllowed = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.effectAllowed).to.equal("copy");
            });

            it("When alt / option is pressed, the event arg's dataTransfer.dropEffect is set to `copy`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = false;
                e.altKey = true;
                e.dataTransfer.dropEffect = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.dropEffect).to.equal("copy");
            });

            it("When alt / option is pressed, the event arg's dataTransfer.effectAllowed is set to `copy`.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                // Test condition and signal.
                e.shiftKey = false;
                e.altKey = true;
                e.dataTransfer.effectAllowed = "test-default";

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.dataTransfer.effectAllowed).to.equal("copy");
            });

            it("stopPropagation() is called on its event arg.", /* working */ () => {
                /* Arrange. */
                let target = new DomEl(null, []);

                let dragged = new DomEl(null, []);
                target._dnd.dragged = dragged;
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");
                e.stopWasCalled = false;

                /* Act. */
                target._whenOtherDraggedOverThis(e);

                /* Assert. */
                expect(e.stopWasCalled).to.be.true;
            });

        });
    });

    describe("_whenOtherDraggedToThis()", () => {
        it("preventDefault() is called on its event arg.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);
            target._dnd.dragged = new DomEl(null, []);

            let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

            /* Act. */
            target._whenOtherDraggedToThis(e);

            /* Assert. */
            expect(e.defaultWasPrevented).to.be.true;
        });

        describe("dragged.canDropThisOnto() returns true", () => {
            it("_whenDroppedOntoThis() is called.", /* working */ () => {
                /* Arrange. */
                let dropWasCalled = false;
                let target = new DomEl(null, []);
                target._whenDroppedOntoThis = (_) => {
                    dropWasCalled = true;
                }

                let e = { };
                e.preventDefault = () => {
                };
                e.stopPropagation = () => {
                };

                let dragged = new DomEl(null, []);
                DragAndDrop.singleton.dragged = dragged;

                // The test condition.
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                /* Act. */
                target._whenOtherDraggedToThis(e);

                /* Assert. */
                expect(dropWasCalled).to.be.true;
            });

            it("_whenDroppedOntoThis() is given the method's event arg.", /* working */ () => {
                /* Arrange. */
                let argPassed = null;
                let target = new DomEl(null, []);
                target._whenDroppedOntoThis = (e) => {
                    argPassed = e;
                }

                let e = { description: "Simple object to pass." };
                e.preventDefault = () => {
                };
                e.stopPropagation = () => {
                };

                let dragged = new DomEl(null, []);
                DragAndDrop.singleton.dragged = dragged;

                // The test condition.
                dragged.canDropThisOnto = (_) => {
                    return true;
                };

                /* Act. */
                target._whenOtherDraggedToThis(e);

                /* Assert. */
                expect(argPassed).to.equal(e);
            });

            it("stopPropagation() is called on its event arg.", /* working */ () => {
                /* Arrange. */
                let dragged = new DomEl(null, []);
                DragAndDrop.singleton.dragged = dragged;

                // The test condition.
                dragged.canDropThisOnto = (e) => {
                    return true;
                };

                let target = new DomEl(null, []);
                target._whenDroppedOntoThis = (e) => {
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                /* Act. */
                target._whenOtherDraggedToThis(e);

                /* Assert. */
                expect(e.stopWasCalled).to.be.true;
            });

        });

        describe("dragged.canDropThisOnto() returns false", () => {
            it("_whenDroppedOntoThis() isn't called.", /* working */ () => {
                /* Arrange. */
                let dropWasCalled = false;
                let target = new DomEl(null, []);
                target._whenDroppedOntoThis = (_) => {
                    dropWasCalled = true;
                }

                let e = { };
                e.preventDefault = () => {
                };
                e.stopPropagation = () => {
                };

                let dragged = new DomEl(null, []);
                DragAndDrop.singleton.dragged = dragged;

                // The test condition.
                dragged.canDropThisOnto = (_) => {
                    return false;
                };

                /* Act. */
                target._whenOtherDraggedToThis(e);

                /* Assert. */
                expect(dropWasCalled).to.be.false;
            });

            it("stopPropagation() is not called on its event arg.", /* working */ () => {
                /* Arrange. */
                let dragged = new DomEl(null, []);

                // The test condition.
                dragged.canDropThisOnto = (e) => {
                    return false;
                };
                DragAndDrop.singleton.dragged = dragged;

                // let stopWasCalled = false;
                let target = new DomEl(null, []);
                target._whenDroppedOntoThis = (e) => {
                };

                let e = DomElFixtures.createMinimalSpoofEventArg("event-target");

                /* Act. */
                target._whenOtherDraggedToThis(e);

                /* Assert. */
                expect(e.stopWasCalled).to.be.false;
            });
        });
    });

    describe("_addToParent()", () => {
        it("When this DomEl was the one dragged, nothing is changed.", /* working */ () => {
            /* This tests the case of an object dragged onto itself. */

            /* Arrange. */
            let parent = new DomEl(null, []);
            let target = new DomEl(parent, []);

            // Test condition.
            DragAndDrop.singleton.dragged = target;

            let e = { altKey: false, shiftKey: false };

            // Test indicators.
            let childrenBefore = [ ...parent.children ];
            let parentIdBefore = target.parent.id;

            /* Act. */
            target._addToParent(e);

            /* Arrange. */
            expect(parent.children).to.deep.equal(childrenBefore);
            expect(target.parent.id).to.equal(parentIdBefore);
        });

        it("When this is a middle child, the dragged DomEl is added before this one.", /* working */ () => {
            /* Arrange. */
            // Test condition: middle child.
            let parent = new DomEl(null, []);
            let firstChild = new DomEl(parent, []);
            let target = new DomEl(parent, []);
            let thirdChild = new DomEl(parent, []);

            // Test condition: different dragged DomEl.
            let source = new DomEl(null, []);
            let dragged = new DomEl(source, []);
            DragAndDrop.singleton.dragged = dragged;

            let e = { altKey: false, shiftKey: false };

            // Test indicators.
            let expectedChildren = [ firstChild, dragged, target, thirdChild ];

            /* Act. */
            target._addToParent(e);

            /* Arrange. */
            expect(parent.children).to.deep.equal(expectedChildren);
        });

        it("When this is the first child, the dragged DomEl is added as the first child.", /* working */ () => {
            /* Arrange. */
            // Test condition: first child.
            let parent = new DomEl(null, []);
            let target = new DomEl(parent, []);
            let secondChild = new DomEl(parent, []);

            // Test condition: different dragged DomEl.
            let source = new DomEl(null, []);
            let dragged = new DomEl(source, []);
            DragAndDrop.singleton.dragged = dragged;

            let e = { altKey: false, shiftKey: false };

            // Test indicators.
            let expectedChildren = [ dragged, target, secondChild ];

            /* Act. */
            target._addToParent(e);

            /* Arrange. */
            expect(parent.children).to.deep.equal(expectedChildren);
        });

        it("When this is the last child, it remains the last child after the dragged DomEl is added.", /* working */ () => {
            /* Arrange. */
            // Test condition: preceding children.
            let parent = new DomEl(null, []);
            let firstChild = new DomEl(parent, []);
            let secondChild = new DomEl(parent, []);
            let target = new DomEl(parent, []);

            // Test condition: different dragged DomEl.
            let source = new DomEl(null, []);
            let dragged = new DomEl(source, []);
            DragAndDrop.singleton.dragged = dragged;

            let e = { altKey: false, shiftKey: false };

            // Test indicators.
            let expectedChildren = [ firstChild, secondChild, dragged, target ];

            /* Act. */
            target._addToParent(e);

            /* Assert. */
            expect(parent.children).to.deep.equal(expectedChildren);
        });

        describe("Neither shift nor alt is pressed", () => {
            it("The dragged DomEl is removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { altKey: false, shiftKey: false };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addToParent(e);

                /* Assert. */
                expect(previous.children).to.be.empty;
            });

            it("The added DomEl is the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { altKey: false, shiftKey: false };

                /* Act. */
                target._addToParent(e);

                /* Assemble. */
                let actual = parent.children[0];

                /* Assert. */
                // Same object.
                expect(actual).to.equal(dragged);
            });

            it("The original dragged DomEl is returned.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { altKey: false, shiftKey: false };

                /* Act. */
                let actual = target._addToParent(e);

                /* Assert. */
                // Same object.
                expect(actual).to.equal(dragged);
            });
        });

        describe("Shift is pressed.", () => {
            it("The dragged DomEl isn't removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { altKey: false, shiftKey: true };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addToParent(e);

                /* Assert. */
                expect(previous.children).to.deep.equal([ dragged ]);
            });

            it("The added DomEl is a clone of the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { altKey: false, shiftKey: true };

                /* Act. */
                target._addToParent(e);

                /* Assemble. */
                let actual = parent.children[0];

                // These differ in a clone, so they 
                // are removed for easy comparing.
                delete dragged._id;
                delete actual._id;

                dragged.domNode.id = "replaced";
                actual.domNode.id = "replaced";

                delete dragged._parent;
                delete actual._parent;

                delete dragged._eventListeners;
                delete actual._eventListeners;

                /* Assert. */
                // Different object, (almost) identical contents.
                expect(actual).not.to.equal(dragged);
                expect(actual).to.deep.equal(dragged);
            });

            it("The cloned DomEl is returned.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { altKey: false, shiftKey: true };

                /* Act. */
                let actual = target._addToParent(e);

                /* Assemble. */
                // Parent's first child should be the clone.
                let expected = parent.children[0];

                /* Assert. */
                // Same object as the clone
                expect(actual).to.equal(expected);

                // Different object than `dragged`.
                expect(actual).not.to.equal(dragged);
            });
        });

        describe("Alt is pressed.", () => {
            it("The dragged DomEl isn't removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { altKey: true, shiftKey: false };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addToParent(e);

                /* Assert. */
                expect(previous.children).to.deep.equal([ dragged ]);
            });

            it("The added DomEl is a clone of the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { altKey: true, shiftKey: false };

                /* Act. */
                target._addToParent(e);

                /* Assemble. */
                let actual = parent.children[0];

                // These differ in a clone, so they 
                // are removed for easy comparing.
                delete dragged._id;
                delete actual._id;

                dragged.domNode.id = "replaced";
                actual.domNode.id = "replaced";

                delete dragged._parent;
                delete actual._parent;

                delete dragged._eventListeners;
                delete actual._eventListeners;

                /* Assert. */
                // Different object, (almost) identical contents.
                expect(actual).not.to.equal(dragged);
                expect(actual).to.deep.equal(dragged);
            });

            it("The cloned DomEl is returned.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { altKey: true, shiftKey: false };

                /* Act. */
                let actual = target._addToParent(e);

                /* Assemble. */
                // Parent's first child should be the clone.
                let expected = parent.children[0];

                /* Assert. */
                // Same object as the clone.
                expect(actual).to.equal(expected);

                // Different object than `dragged`.
                expect(actual).not.to.equal(dragged);
            });
        });
    });

    describe("_addAsChild()", () => {
        it("When this DomEl has no children, the dragged DomEl is added as its only child.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();

            // Test condition.
            let target = new DomEl(root, []);

            let parent = new DomEl();
            let dragged = new DomEl(parent);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            DragAndDrop.singleton.dragged = dragged;

            let e = { altKey: false, shiftKey: false };

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ dragged ]);
        });

        it("When the dragged DomEl is dropped before the first child, it's added as the new first child.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(3);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let top = DomElFixtures.getTopOfDomNode(children[0]);
            let e = { clientY: top - 1, altKey: false, shiftKey: false };

            let parent = new DomEl();
            let dragged = new DomEl(parent);
            DragAndDrop.singleton.dragged = dragged;

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ dragged, ...children ]);
        });

        it("When the dragged DomEl is dropped after the last child, it's added as the new last child.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(3);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let last = children[children.length - 1];
            let top = DomElFixtures.getTopOfDomNode(last);
            let far = 10000;
            let e = { clientY: top + far, altKey: false, shiftKey: false };

            let parent = new DomEl();
            let dragged = new DomEl(parent);
            DragAndDrop.singleton.dragged = dragged;

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ ...children, dragged ]);
        });

        it("When the dragged DomEl is dropped between two children, it's added between them.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(6);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let at = target.children.length / 2;
            let middle = target.children[at];
            let top = DomElFixtures.getTopOfDomNode(middle);

            let e = { clientY: top - 1, altKey: false, shiftKey: false };

            let parent = new DomEl();
            let dragged = new DomEl(parent);
            DragAndDrop.singleton.dragged = dragged;

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            let expecteds = [ ...children.slice(0, at), dragged, ...children.slice(at) ];
            expect(target.children).to.deep.equal(expecteds);
        });

        it("When the dragged DomEl is dropped above itself as the first child, nothing changes.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(6);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let dragged = target.children[0];
            DragAndDrop.singleton.dragged = dragged;
            let top = DomElFixtures.getTopOfDomNode(dragged);
            let e = { clientY: top - 1, altKey: false, shiftKey: false };

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ ...children ]);
        });

        it("When the dragged DomEl is dropped above itself and after another child, nothing changes.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(6);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let dragged = target.children[3];
            DragAndDrop.singleton.dragged = dragged;
            let top = DomElFixtures.getTopOfDomNode(dragged);
            let e = { clientY: top - 1, altKey: false, shiftKey: false };

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ ...children ]);
        });

        it("When the dragged DomEl is dropped below itself and before another child, nothing changes.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(6);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let at = 3;
            let next = at + 1;

            let dragged = target.children[at];
            DragAndDrop.singleton.dragged = dragged;

            let top = DomElFixtures.getTopOfDomNode(target.children[next]);
            let e = { clientY: top - 1, altKey: false, shiftKey: false };

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ ...children ]);
        });

        it("When the dragged DomEl is dropped below itself as the last child, nothing changes.", /* working */ () => {
            /* Arrange. */
            let root = new DomEl();
            let target = new DomEl(root);
            let children = DomElFixtures.createMinimalChildren(6);
            DomElFixtures.addChildrenToDomEl(target, children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let dragged = target.children[5];
            DragAndDrop.singleton.dragged = dragged;

            let top = DomElFixtures.getTopOfDomNode(dragged);
            let far = 10000;
            let e = { clientY: top + far, altKey: false, shiftKey: false };

            /* Act. */
            target._addAsChild(e);

            /* Assert. */
            expect(target.children).to.deep.equal([ ...children ]);
        });

        describe("Neither shift nor alt is pressed", () => {
            it("The dragged DomEl is removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { clientY: 0, altKey: false, shiftKey: false };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addAsChild(e);

                /* Assert. */
                expect(previous.children).to.be.empty;
            });

            it("The added DomEl is the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { clientY: 0, altKey: false, shiftKey: false };

                /* Act. */
                target._addAsChild(e);

                /* Assemble. */
                let actual = target.children[0];

                /* Assert. */
                // Same object.
                expect(actual).to.equal(dragged);
            });

            it("The original dragged DomEl is returned.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                target._dnd.dragged = dragged;

                // Test condition: neither shift nor alt.
                let e = { clientY: 0, altKey: false, shiftKey: false };

                /* Act. */
                let actual = target._addAsChild(e);

                /* Assert. */
                // Same object.
                expect(actual).to.equal(dragged);
            });
        });

        describe("Shift is pressed.", () => {
            it("The dragged DomEl isn't removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { clientY: 0, altKey: false, shiftKey: true };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addAsChild(e);

                /* Assert. */
                expect(previous.children).to.deep.equal([ dragged ]);
            });

            it("The added DomEl is a clone of the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { clientY: 0, altKey: false, shiftKey: true };

                /* Act. */
                target._addAsChild(e);

                /* Assemble. */
                let actual = target.children[0];

                // These differ in a clone, so they 
                // are removed for easy comparing.
                delete dragged._id;
                delete actual._id;

                dragged.domNode.id = "replaced";
                actual.domNode.id = "replaced";

                delete dragged._parent;
                delete actual._parent;

                delete dragged._eventListeners;
                delete actual._eventListeners;

                /* Assert. */
                // Different object, (almost) identical contents.
                expect(actual).not.to.equal(dragged);
                expect(actual).to.deep.equal(dragged);
            });

            it("The cloned DomEl is returned.", () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: shift, but not alt.
                let e = { clientY: 0, altKey: false, shiftKey: true };

                /* Act. */
                let actual = target._addAsChild(e);

                /* Assemble. */
                // Target's first child should be the clone.
                let expected = target.children[0];

                /* Assert. */
                // Same object as the clone.
                expect(actual).to.equal(expected);
                
                // Different object than `dragged`.
                expect(actual).not.to.equal(dragged);
            });
        });

        describe("Alt is pressed.", () => {
            it("The dragged DomEl isn't removed from its old parent.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { clientY: 0, altKey: true, shiftKey: false };

                /* Ascertain. */
                expect(previous.children).to.deep.equal([ dragged ]);

                /* Act. */
                target._addAsChild(e);

                /* Assert. */
                expect(previous.children).to.deep.equal([ dragged ]);
            });

            it("The added DomEl is a clone of the original dragged DomEl.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { clientY: 0, altKey: true, shiftKey: false };

                /* Act. */
                target._addAsChild(e);

                /* Assemble. */
                let actual = target.children[0];

                // These differ in a clone, so they 
                // are removed for easy comparing.
                delete dragged._id;
                delete actual._id;

                dragged.domNode.id = "replaced";
                actual.domNode.id = "replaced";

                delete dragged._parent;
                delete actual._parent;

                delete dragged._eventListeners;
                delete actual._eventListeners;

                /* Assert. */
                // Different object, (almost) identical contents.
                expect(actual).not.to.equal(dragged);
                expect(actual).to.deep.equal(dragged);
            });

            it("The cloned DomEl is returned.", () => {
                /* Arrange. */
                let parent = new DomEl();
                let target = new DomEl(parent);
                let children = DomElFixtures.createMinimalChildren(6);
                DomElFixtures.addChildrenToDomEl(target, children);

                // Defining screen coordinates.
                target.display();
                DomElFixtures.spoofScreenLocations(target.children);

                let previous = new DomEl();
                let dragged = new DomEl(previous);
                DragAndDrop.singleton.dragged = dragged;

                // Test condition: alt, but not shift.
                let e = { clientY: 0, altKey: true, shiftKey: false };

                /* Act. */
                let actual = target._addAsChild(e);

                /* Assemble. */
                // Target's first child should be the clone.
                let expected = target.children[0];

                /* Assert. */
                // Same object as the clone.
                expect(actual).to.equal(expected);
                
                // Different object than `dragged`.
                expect(actual).not.to.equal(dragged);
            });
        });

    });

    describe("_calculateDragged()", () => {
        it("If neither shift nor alt is pressed, DragAndDrop.singleton.dragged is returned.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test condition, spoofed event: no alt or shift.
            let e = { altKey: false, shiftKey: false };

            // Test indicator.
            let expected = { nature: "test object" };
            target._dnd.dragged = expected;

            /* Act. */
            let actual = target._calculateDragged(e);

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("If shift is pressed, a clone of DragAndDrop.singleton.dragged is returned.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test condition, spoofed event: shift, not alt.
            let e = { altKey: false, shiftKey: true };

            // Test indicator.
            let expected = { nature: "spoofed clone" };
            let dragged = { nature: "spoof dragged" };
            dragged.clone = () => {
                return expected;
            };

            target._dnd.dragged = dragged;

            /* Act. */
            let actual = target._calculateDragged(e);

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("If alt is pressed, a clone of DragAndDrop.singleton.dragged is returned.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test condition, spoofed event: alt, not shift.
            let e = { altKey: true, shiftKey: false };

            // Test indicator.
            let expected = { nature: "spoofed clone" };
            let dragged = { nature: "spoof dragged" };
            dragged.clone = () => {
                return expected;
            };

            target._dnd.dragged = dragged;

            /* Act. */
            let actual = target._calculateDragged(e);

            /* Assert. */
            expect(actual).to.equal(expected);
        });

    });

    describe("_calculateOntoChild()", () => {
        it("When there are no existing children, null is returned.", /* working */ () => {
            /* Arrange. */
            // Test condition.
            let target = new DomEl(new DomEl(), []);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // All Y positions act the same here.
            let e = { clientY: 10 };

            /* Act. */
            let actual = target._calculateOntoChild(e);

            /* Assert. */
            expect(actual).to.be.null;
        });

        it("When the dragged DomEl is dropped below the last child, null is returned.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(new DomEl(), children);
            let expected = children[children.length - 1];

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.  Far below the last child.
            let top = DomElFixtures.getTopOfDomNode(expected);
            let far = 10000;
            let e = { clientY: top + far };

            /* Act. */
            let actual = target._calculateOntoChild(e);

            /* Assert. */
            expect(actual).to.be.null;
        });

        it("When the dragged DomEl is dropped before the first child, the first child is returned.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(new DomEl(), children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.
            let top = DomElFixtures.getTopOfDomNode(children[0]);
            let e = { clientY: top - 1 };

            /* Act. */
            let actual = target._calculateOntoChild(e);

            /* Assert. */
            expect(actual).to.equal(children[0]);
        });

        it("When the dragged DomEl is dropped between two children, the child after is returned.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(new DomEl(), children);

            // Defining screen coordinates.
            target.display();
            DomElFixtures.spoofScreenLocations(target.children);

            // Test condition.  Between [3] and [4].
            let at = 4;
            let top = DomElFixtures.getTopOfDomNode(children[at]);
            let e = { clientY: top - 1 };

            /* Act. */
            let actual = target._calculateOntoChild(e);

            /* Assert. */
            expect(actual).to.equal(children[at]);
        });

    });

    describe("_removeDraggedFromParentIfMoving()", () => {
        it("When shift is pressed, removeChild() isn't called on .dragged's .parent.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test indicator.
            let wasCalled = false;
            let parent = { };
            parent.removeChild = () => {
                wasCalled = true;
            };

            let dragged = { };
            dragged.parent = parent;
            target._dnd.dragged = dragged;

            // Test condition.
            let e = { altKey: false, shiftKey: true };

            /* Act. */
            target._removeDraggedFromParentIfMoving(e, dragged);

            /* Assert. */
            expect(wasCalled).to.be.false;
        });

        it("When alt is pressed, removeChild() isn't called on .dragged's .parent.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test indicator.
            let wasCalled = false;
            let parent = { };
            parent.removeChild = () => {
                wasCalled = true;
            };

            let dragged = { };
            dragged.parent = parent;
            target._dnd.dragged = dragged;

            // Test condition.
            let e = { altKey: true, shiftKey: false };

            /* Act. */
            target._removeDraggedFromParentIfMoving(e, dragged);

            /* Assert. */
            expect(wasCalled).to.be.false;
        });

        it("When neither shift nor alt is pressed, removeChild() is called on .dragged's .parent.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test indicator.
            let wasCalled = false;
            let parent = { };
            parent.removeChild = () => {
                wasCalled = true;
            };

            let dragged = { };
            dragged.parent = parent;
            target._dnd.dragged = dragged;
            
            dragged.domNode = { };
            dragged.domNode.remove = () => { };

            // Test condition.
            let e = { altKey: false, shiftKey: false };

            /* Act. */
            target._removeDraggedFromParentIfMoving(e, dragged);

            /* Assert. */
            expect(wasCalled).to.be.true;
        });

        it("When neither shift nor alt is pressed, the removeChild() call is given .dragged as arg.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            // Test indicator.
            let actual = null;
            let parent = { };
            parent.removeChild = (arg) => {
                actual = arg;
            };

            let dragged = { };
            dragged.parent = parent;
            target._dnd.dragged = dragged;
            
            dragged.domNode = { };
            dragged.domNode.remove = () => { };

            // Test condition.
            let e = { altKey: false, shiftKey: false };

            /* Act. */
            target._removeDraggedFromParentIfMoving(e, dragged);

            /* Assert. */
            expect(actual).to.equal(dragged);
        });

        it("When neither shift nor alt is pressed, .dragged.domNode.remove() is called", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);

            let parent = { };
            parent.removeChild = () => { };

            let dragged = { };
            dragged.parent = parent;
            target._dnd.dragged = dragged;
            
            // Test indicator.
            let wasCalled = false;
            dragged.domNode = { };
            dragged.domNode.remove = () => { wasCalled = true; };

            // Test condition.
            let e = { altKey: false, shiftKey: false };

            /* Act. */
            target._removeDraggedFromParentIfMoving(e, dragged);

            /* Assert. */
            expect(wasCalled).to.be.true;
        });

    });

});

