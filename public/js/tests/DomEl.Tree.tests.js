/**/

import { expect } from "@esm-bundle/chai";

import { DomEl } from "../DomEl.js";
import { NewDom } from "../NewDom.js";
import { GroupEl } from "../GroupEl.js";
import { DragAndDrop } from "../DragAndDrop.js";

import { DomElFixtures } from "./fixtures/DomEl.fixtures.js";

describe("DomEl — tree", () => {
    describe("addChild()", () => {
        it("The new child is inserted before the given child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let target = new DomEl(null, children);

            let expected = DomElFixtures.createMinimalChild();
            let at = 2;
            let before = children[at];

            /* Act. */
            target.addChild(expected, before);

            /* Assemble. */
            let actual = target.children[at];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("After insertion, existing children are still in place.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(6);
            let target = new DomEl(null, children);

            let added = DomElFixtures.createMinimalChild();
            let expecteds = [ children[0], children[1], children[2], added, children[3], children[4], children[5] ];
            let at = 3;
            let before = children[at];

            /* Act. */
            target.addChild(added, before);

            /* Assemble. */
            let actuals = target.children;

            /* Assert. */
            expect(actuals.length).to.equal(expecteds.length);

            for (let at = 0; at < actuals.length; at++) {
                let actual = actuals[at];
                let expected = expecteds[at];
                expect(actual).to.equal(expected);
            }
        });

        it("When there are no existing children, the new child becomes the sole child.", /* working */ () => {
            /* Arrange. */
            // Last arg means no children for both.
            let target = new DomEl(null, []);

            let expected = DomElFixtures.createMinimalChild();

            /* Act. */
            target.addChild(expected);

            /* Assemble. */
            let actual = target.children[0];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("When no existing child is given, the new child is added as the last child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);

            let expected = DomElFixtures.createMinimalChild();

            /* Act. */
            // No `before` arg.
            target.addChild(expected);

            /* Assemble. */
            let top = target.children.length - 1;
            let actual = target.children[top];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("When the given existing child is null, the new child is added as the last child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);

            let expected = DomElFixtures.createMinimalChild();
            let before = null;

            /* Act. */
            target.addChild(expected, before);

            /* Assemble. */
            let top = target.children.length - 1;
            let actual = target.children[top];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("When the given existing child is not a child of the target DomEl, the new child is added as the last child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);

            let expected = DomElFixtures.createMinimalChild();
            
            // Test condition.
            let before = DomElFixtures.createMinimalChild();

            /* Act. */
            target.addChild(expected, before);

            /* Assemble. */
            let top = target.children.length - 1;
            let actual = target.children[top];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("When the given existing child is the first child, the new child is added as the first child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);

            let expected = DomElFixtures.createMinimalChild();
            let before = children[0];

            /* Act. */
            target.addChild(expected, before);

            /* Assemble. */
            let actual = target.children[0];

            /* Assert. */
            expect(actual).to.equal(expected);
        });

        it("If the DomEL's .isDisplayed is set, display() is not called on the child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let target = new DomEl(null, children);

            let added = DomElFixtures.createMinimalChild();

            // Test condition.
            target.isDisplayed = true;

            // Indicator.
            let displayWasCalled = false;

            // And its setting spoof method.
            added.display = () => {
                displayWasCalled = true;
            }

            /* Act. */
            target.addChild(added, 2);

            /* Assert. */
            expect(displayWasCalled).to.be.false;
        });

        it("If the DomEl's .isDisplayed is cleared, display() is not called on the child.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let target = new DomEl(null, children);

            let added = DomElFixtures.createMinimalChild();

            // Test condition.
            target.isDisplayed = false;

            // Indicator.
            let displayWasCalled = false;

            // And its setting spoof method.
            added.display = () => {
                displayWasCalled = true;
            }

            /* Act. */
            target.addChild(added, 2);

            /* Assert. */
            expect(displayWasCalled).to.be.false;
        });

    });
    
    describe("removeChild()", () => {
        it("The given child is removed from .children.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(null, children);

            let removed = target.children[5];

            /* Act. */
            target.removeChild(removed);

            /* Assemble. */
            let actual = target.children.includes(removed);

            /* Assert. */
            expect(actual).to.be.false;
        });

        it("The given child no longer has this DomEl as .parent.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(null, children);

            let removed = target.children[5];

            /* Ascertain. */
            expect(removed.parent).to.equal(target);

            /* Act. */
            target.removeChild(removed);

            /* Assert. */
            expect(removed.parent).to.be.null;
        });
        
        it("After removal, the other children are still present.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(4);

            // Children are unwrapped to a new array to simplify later.
            let target = new DomEl(null, [ ...children ]);

            let removed = target.children[1];

            /* Act. */
            target.removeChild(removed);

            /* Assemble. */
            let expecteds = [ children[0], children[2], children[3] ];
            let actuals = target.children;

            /* Assert. */
            // Equality of arrays.
            expect(actuals).to.deep.equal(expecteds);
        });

        it("remove() is not called on the removed child's .domNode.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);
            let removed = target.children[6];

            let wasRemoved = false;
            removed.domNode.remove = () => {
                wasRemoved = true;
            };

            /* Act. */
            target.removeChild(removed);

            /* Assert. */
            expect(wasRemoved).to.be.false;
        });

        it("If the removed child's .domNode was in the DOM, it is removed from the DOM.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl(null, []);
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(parent, children);

            // Test condition: Adding all 
            // children's DOM nodes to DOM.
            target.display();

            let removed = target.children[6];

            /* Ascertain. */
            for (let child of children) {
                let node = child.domNode;
                expect(target.domNode.children).to.contain(node);
            }

            /* Act. */
            target.removeChild(removed);

            /* Assert. */
            expect(target.domNode.children).to.not.contain(removed.domNode);
        });

        it("If the removed child's .domNode wasn't in the DOM, nothing is changed.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl(null, []);
            let children = DomElFixtures.createMinimalChildren(10);

            let target = new DomEl(parent, children);

            /* Test condition: No children's DOM noded 
             * in DOM because display() was not called. */

            let removed = target.children[6];

            /* Ascertain. */
            for (let child of children) {
                let node = child.domNode;
                expect(target.domNode.children).to.not.contain(node);
            }

            /* Act. */
            target.removeChild(removed);

            /* Assert. */
            expect(target.domNode.children).to.not.contain(removed.domNode);
        });

        it("The removed child's destructor() is not called.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);
            let removed = target.children[6];

            let wasDestroyed = false;
            removed.destructor = () => {
                wasDestroyed = true;
            }

            /* Act. */
            target.removeChild(removed);

            /* Assert. */
            expect(wasDestroyed).to.be.false;
        });

    });

    describe("subtractChild()", () => {
        it("The given child is removed from .children.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(7);
            let target = new DomEl(null, children);

            let removed = target.children[5];

            /* Act. */
            target.subtractChild(removed);

            /* Assemble. */
            let actual = target.children.includes(removed);

            /* Assert. */
            expect(actual).to.be.false;
        });

        it("After subtraction, the other children are still present.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(4);

            // Children are unwrapped to a new array to simplify later.
            let target = new DomEl(null, [ ...children ]);

            let removed = target.children[1];

            /* Act. */
            target.subtractChild(removed);

            /* Assemble. */
            let expecteds = [ children[0], children[2], children[3] ];
            let actuals = target.children;

            /* Assert. */
            // Equality of arrays.
            expect(actuals).to.deep.equal(expecteds);
        });

        it("remove() is called on the subtracted child's .domNode.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);
            let removed = target.children[6];

            let wasRemoved = false;
            removed.domNode.remove = () => {
                wasRemoved = true;
            };

            /* Act. */
            target.subtractChild(removed);

            /* Assert. */
            expect(wasRemoved).to.be.true;
        });

        it("If the subtracted child's .domNode was in the DOM, it is removed from the DOM.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl(null, []);
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(parent, children);

            // Test condition: Adding all 
            // children's DOM nodes to DOM.
            target.display();

            let removed = target.children[6];

            /* Ascertain. */
            for (let child of children) {
                let node = child.domNode;
                expect(target.domNode.children).to.contain(node);
            }

            /* Act. */
            target.subtractChild(removed);

            /* Assert. */
            expect(target.domNode.children).to.not.contain(removed.domNode);
        });

        it("If the subtracted child's .domNode wasn't in the DOM, nothing is changed.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl(null, []);
            let children = DomElFixtures.createMinimalChildren(10);

            let target = new DomEl(parent, children);

            /* Test condition: No children's DOM noded 
             * in DOM because display() was not called. */

            let removed = target.children[6];

            /* Ascertain. */
            for (let child of children) {
                let node = child.domNode;
                expect(target.domNode.children).to.not.contain(node);
            }

            /* Act. */
            target.subtractChild(removed);

            /* Assert. */
            expect(target.domNode.children).to.not.contain(removed.domNode);
        });

        it("The removed child's destructor() is called.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let target = new DomEl(null, children);
            let removed = target.children[7];

            let wasDestroyed = false;
            removed.destructor = () => {
                wasDestroyed = true;
            };

            /* Act. */
            target.subtractChild(removed);

            /* Assert. */
            expect(wasDestroyed).to.be.true;
        });

    });
    
    describe("clone()", () => {
        it("A new instance has the same class / subclass as the old one.", /* working */ () => {
            /* Arrange. */
            // Test condition: A trivial subclass.
            class Subclass extends DomEl {
                /* No redefinitions. */
            }

            let children = DomElFixtures.createMinimalChildren(3);
            let parent = new Subclass(null, []);
            let target = new Subclass(parent, children);

            /* Act. */
            let actual = target.clone();

            /* Assert. */
            expect(actual).instanceOf(Subclass);
        });

        it("All non-DOM, non-children contents are duplicated except .parent, .id, and ._eventListeners.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let parent = new DomEl(null, []);
            let target = new DomEl(parent, children);

            // Applying a realistic-enough DOM node subtree.
            target._domNode.innerHTML = DomElFixtures.createSimpleDomSubtree();

            /* Act. */
            let actual = target.clone();

            /* Assemble. */
            // Removing the members that are different (._id, ._eventListeners) 
            // or not compared in this test (._parent, ._domNode, ._children).
            delete target._id;
            delete actual._id;

            delete target._domNode;
            delete actual._domNode;
            
            delete target._parent;
            delete actual._parent;

            delete target._children;
            delete actual._children;

            delete target._eventListeners;
            delete actual._eventListeners;

            /* Assert. */
            // Different objects, but with 
            // all targeted contents equal.
            expect(actual).not.to.equal(target);
            expect(actual).to.deep.equal(target);
        });

        it("The new DomEl's .id and .domNode.id are different than the old one's.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let parent = new DomEl(null, []);
            let target = new DomEl(parent, children);

            /* Act. */
            let actual = target.clone();

            /* Assert. */
            expect(actual.id).not.to.equal(target.id);
            expect(actual.domNode.id).not.to.equal(target.domNode.id);
        });

        describe("No .children.", () => {
            it("All DOM contents are duplicated except .domNode.id.", /* working */ () => {
                /* Arrange. */
                let parent = new DomEl(null, []);
                let target = new DomEl(parent, []);

                // Applying a realistic-enough DOM node subtree.
                target._domNode.innerHTML = DomElFixtures.createSimpleDomSubtree();

                /* Act. */
                let actual = target.clone();

                /* Assemble. */
                // Replacing the els' .ids, 
                // which should be different.
                actual.domNode.id = "replaced";
                target.domNode.id = "replaced";

                /* Assert. */
                // Different objects, but with all contents equal.
                expect(actual.domNode).to.deep.equal(target.domNode);
            });
        });

        describe("One or more .children.", () => {
            it(".stencil is used for .domNode", /* working */ () => {
                /* Arrange. */
                let children = DomElFixtures.createMinimalChildren(3);
                let parent = new DomEl(null, []);
                let target = new DomEl(parent, children);

                let expected = target.stencil;

                // Applying a new DOM subtree 
                // that shouldn't be cloned.
                target._domNode = DomElFixtures.createSimpleDomSubtree();

                /* Act. */
                let actual = target.clone();

                /* Assemble. */
                // Removing the id values and original DomEl's 
                // .children for simplest content comparisons.
                actual.domNode.id = "replaced";
                expected.id = "replaced";
                expected.classList.add("DomEl");

                /* Assert. */
                expect(actual.domNode).to.deep.equal(expected);
            });

            it(".domNode's .innerHTML is not duplicated.", /* working */ () => {
                /* Arrange. */
                let child = DomElFixtures.createMinimalChild();
                let parent = new DomEl(null, []);
                let target = new DomEl(parent, [ child ]);

                // Applying a realistic-enough DOM node subtree.
                target._domNode.innerHTML = DomElFixtures.createSimpleDomSubtree();

                /* Act. */
                let clone = target.clone();

                /* Assemble. */
                let actual = clone.domNode.innerHTML;

                /* Assert. */
                // Different objects, but with all contents equal.
                expect(actual).not.to.equal(target.domNode.innerHTML);
            });

            it("DomEls in .children are all cloned with clone() on each one and then addChild() of the clone.", /* working */ () => {
                /* Arrange. */
                let children = DomElFixtures.createMinimalChildren(1);
                let parent = new DomEl(null, []);
                let target = new DomEl(parent, children);

                // Applying a realistic-enough DOM node subtree.
                target._domNode.innerHTML = DomElFixtures.createSimpleDomSubtree();

                /* Act. */
                let actual = target.clone();

                /* Assert. */
                // Different objects, but with all targeted contents equal.
                for (let of = 0; of < target.children.length; of++) {
                    let expChild = target.children[of];
                    let acChild = actual.children[of];

                    // Removing values that should be unique even after cloning, 
                    // or should not be set at all when cloning.
                    delete expChild._id;
                    delete acChild._id;
                    expChild.domNode.id = "replaced";
                    acChild.domNode.id = "replaced";

                    delete expChild._parent;
                    delete acChild._parent;

                    delete expChild._eventListeners;
                    delete acChild._eventListeners;

                    // Different but (almost) identical objects.
                    expect(acChild).not.to.equal(expChild);
                    expect(acChild).to.deep.equal(expChild);
                }
            });

        });

    });

});

