/**/

import { expect } from "@esm-bundle/chai";

import { DomEl } from "../DomEl.js";
import { NewDom } from "../NewDom.js";
import { GroupEl } from "../GroupEl.js";
import { DragAndDrop } from "../DragAndDrop.js";

import { DomElFixtures } from "./fixtures/DomEl.fixtures.js";

describe("DomEl — displaying", () => {
    describe("display()", () => {
        it(".isDisplayed is set to true.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl(null, []);
            target._displaySelf = () => {
            };
            target._displayChildren = () => {
            };

            /* Act. */
            target.display();

            /* Assert. */
            expect(target.isDisplayed).to.be.true;
        });

        it("_displaySelf() is called.", /* working */ () => {
            /* Arrange. */
            let didDisplaySelf = false;
            let target = new DomEl(null, []);

            target._displaySelf = () => {
                didDisplaySelf = true;
            };

            /* Act. */
            target.display();

            /* Assert. */
            expect(didDisplaySelf).to.be.true;
        });

        it("_displayChildren() is called.", /* working */ () => {
            /* Arrange. */
            let didDisplayChildren = false;
            let target = new DomEl(null, []);

            target._displaySelf = () => {
            };

            target._displayChildren = () => {
                didDisplayChildren = true;
            };

            /* Act. */
            target.display();

            /* Assert. */
            expect(didDisplayChildren).to.be.true;
        });
    });

    describe("_displaySelf()", () => {
        it("The DomEl's .domNode is inserted into its parent's .domNode.children at its index.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(4);

            // DomEl and DOM have equivalent children.
            let parent = new DomEl(null, [ ...children ]);

            for (let child of children) {
                parent.domNode.appendChild(child.domNode);
            }

            let at = 2;
            let target = parent.children[at];

            /* Act. */
            target._displaySelf();

            /* Assemble. */
            let actuals = parent.domNode.children;

            /* Assert. */
            expect(actuals[at]).to.equal(target.domNode);
        });

        it("If this DomEl is the only child, its .domNode is added as the only element in its parent's .domNode.children.", /* working */ () => {
            /* Arrange. */
            // Test condition: No other children.
            let target = DomElFixtures.createMinimalChild();
            let parent = new DomEl(null, [ target ]);
            
            /* Act. */
            target._displaySelf();

            /* Assemble. */
            let actuals = parent.domNode.children;

            /* Assert. */
            expect(actuals[0]).to.equal(target.domNode);
        });

        it("If this DomEl's .domNode is already a child of its parent's .domNode, nothing changes.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(10);
            let parent = new DomEl(null, [ ...children ]);

            let target = parent.children[6];

            // Test condition: Child already in parent's DOM subtree.
            for (let child of children) {
                parent.domNode.appendChild(child.domNode);
            }

            let expecteds = Array.from(parent.domNode.children);

            /* Act. */
            target._displaySelf();

            /* Assemble. */
            let actuals = Array.from(parent.domNode.children);

            /* Assert. */
            expect(actuals).to.deep.equal(expecteds);
        });
    });

});

