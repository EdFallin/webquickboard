/**/

import { expect } from "@esm-bundle/chai";

import { DomEl } from "../DomEl.js";
import { NewDom } from "../NewDom.js";
import { GroupEl } from "../GroupEl.js";
import { DragAndDrop } from "../DragAndDrop.js";

import { DomElFixtures } from "./fixtures/DomEl.fixtures.js";

describe("DomEl — ontology", () => {
    describe("constructor()", () => {
        describe("_setBasicInstanceMembers() and its operations", () => {
            it("_setBasicInstanceMembers() is called.", /* working */ () => {
                /* Arrange. */
                // Temporary method redefinition to set an indicator.
                let instanceBasicsWereSet = false;
                let real_setBasicInstanceMembers = DomEl.prototype._setBasicInstanceMembers;
                let real_linkChildrenToSelf = DomEl.prototype._linkChildrenToSelf;
                let real_wireSelfEventListeners = DomEl.prototype._wireSelfEventListeners;

                // Test sign.
                DomEl.prototype._setBasicInstanceMembers = () => {
                    instanceBasicsWereSet = true;
                };

                DomEl.prototype._linkChildrenToSelf = () => {
                };
                
                DomEl.prototype._wireSelfEventListeners = () => { };

                /* Act. */
                let target = new DomEl(null, []);

                /* Aftermath. */
                DomEl.prototype._setBasicInstanceMembers = real_setBasicInstanceMembers;
                DomEl.prototype._linkChildrenToSelf = real_linkChildrenToSelf;
                DomEl.prototype._wireSelfEventListeners = real_wireSelfEventListeners;

                /* Assert. */
                expect(instanceBasicsWereSet).to.be.true;
            });

            it(".id is set to the next value from NewDom.singleton.nextId.", /* working */ () => {
                /* Arrange. */
                let oneLess = NewDom.singleton.nextId;
                let expected = oneLess + 1;

                /* Act. */
                let target = new DomEl(null, []);

                /* Assert. */
                expect(target.id).to.equal(expected);
            });

            it("If no children are provided, .children is set to an empty array.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                // The last arg, not provided here, is any children.
                let target = new DomEl(null);

                /* Assert. */
                // If the array exists, .length is defined.
                expect(target.children.length).to.equal(0);
            });

            it("If children are provided, they are all found in .children.", /* working */ () => {
                /* Arrange. */
                let children = DomElFixtures.createMinimalChildren(6);

                /* Act. */
                let target = new DomEl(null, [ ...children ]);

                /* Assert. */
                // Equality of arrays.
                expect(target.children).to.deep.equal(children);
            });

            it("._dnd is set to DragAndDrop.singleton.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assert. */
                expect(target._dnd).to.equal(DragAndDrop.singleton);
            });
            
            it("._dom is set to NewDom.singleton.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assert. */
                expect(target._dom).to.equal(NewDom.singleton);
            });

            it(".isDisplayed is set to false.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assert. */
                expect(target.isDisplayed).to.be.false;
            });

        });
        
        describe("_linkParentToSelf() and its operations", () => {
            it("_linkParentToSelf() is called.", /* working */ () => {
                /* Arrange. */
                // Temporary method redefinition to set an indicator.
                let wasLinkedToSelf = false;
                let real_linkParentToSelf = DomEl.prototype._linkParentToSelf;
                DomEl.prototype._linkParentToSelf = () => {
                    wasLinkedToSelf = true;
                };

                /* Act. */
                let target = new DomEl(null, []);

                /* Aftermath. */
                // Restoring the normal definition for other tests.
                DomEl.prototype._linkParentToSelf = real_linkParentToSelf;

                /* Assert. */
                expect(wasLinkedToSelf).to.be.true;
            });

            it("If `parent` is a DomEl, addChild() is called on it to add new DomEl as its child.", /* working */ () => {
                /* Arrange. */
                let addChildWasCalled = false;
                let selfAndChild = { self: null, child: null };
                let realAddChild = DomEl.prototype.addChild;
                DomEl.prototype.addChild = function (domEl, at) {
                    addChildWasCalled = true;
                    selfAndChild.self = this;
                    selfAndChild.child = domEl;
                };

                let parent = new DomEl();

                /* Act. */
                let target = new DomEl(parent);

                /* Aftermath. */
                DomEl.prototype.addChild = realAddChild;

                /* Assert. */
                expect(addChildWasCalled).to.be.true;
                expect(selfAndChild.self).to.equal(parent);
                expect(selfAndChild.child).to.equal(target);
            });

            it("If `parent` is not provided, addChild() is not called on it.", /* working */ () => {
                /* Arrange. */
                let addChildWasCalled = false;
                let selfAndChild = { self: null, child: null };
                let realAddChild = DomEl.prototype.addChild;

                DomEl.prototype.addChild = function (domEl, at) {
                    addChildWasCalled = true;
                    selfAndChild.self = this;
                    selfAndChild.child = domEl;
                };

                /* Act. */
                // Test condition: No parent arg.
                let target = new DomEl();

                /* Aftermath. */
                DomEl.prototype.addChild = realAddChild;

                /* Assert. */
                expect(addChildWasCalled).to.be.false;
                expect(selfAndChild.self).to.be.null;
                expect(selfAndChild.child).to.be.null;
            });

            it(".parent is set to the `parent` arg provided.", /* working */ () => {
                /* Arrange. */
                let expected = new DomEl();

                /* Act. */
                let target = new DomEl(expected, []);

                /* Assert. */
                expect(target.parent).to.equal(expected);
            });

        });
        
        describe("_linkChildrenToSelf() and its operations", () => {
            it("_linkChildrenToSelf() is called.", /* working */ () => {
                /* Arrange. */
                // Temporary method redefinition to set an indicator.
                let wereLinkedToSelf = false;
                let real_linkChildrenToSelf = DomEl.prototype._linkChildrenToSelf;
                DomEl.prototype._linkChildrenToSelf = () => {
                    wereLinkedToSelf = true;
                };

                /* Act. */
                let target = new DomEl(null, []);

                /* Aftermath. */
                // Restoring the normal definition for other tests.
                DomEl.prototype._linkChildrenToSelf = real_linkChildrenToSelf;

                /* Assert. */
                expect(wereLinkedToSelf).to.be.true;
            });

            it("Every DomEl in `children` arg has this DomEl as its .parent.", /* working */ () => {
                /* Arrange. */
                // Each of these has "parent" as its initial parent.
                let children = DomElFixtures.createMinimalChildren(3);

                /* Act. */
                let target = new DomEl(null, [ ...children ]);

                /* Assert. */
                for (let child of children) {
                    expect(child.parent).to.equal(target);
                }
            });

        });
        
        describe("_defineEventListeners() and its operations", () => {
            it("_defineEventListeners() is called.", /* working */ () => {
                /* Arrange. */
                // Temporary method redefinition to set a test sign.
                let wasCalled = false;
                let real_defineEventListeners = DomEl.prototype._defineEventListeners;
                DomEl.prototype._defineEventListeners = function() {
                    this._eventListeners = new Map();
                    wasCalled = true;
                };

                /* Act. */
                let target = new DomEl(null, []);

                /* Aftermath. */
                // Restoring the normal definition for other tests.
                DomEl.prototype._defineEventListeners = real_defineEventListeners;

                /* Assert. */
                expect(wasCalled).to.be.true;
            });
            
            it("Listeners are defined on ._eventListeners for all expected drag-and-drop events.", /* working */ () => {
                /* Arrange. */
                let events = [ "dragstart", "dragover", "dragleave", "drop" ];
                let expecteds = [ 
                    DomEl.prototype._whenThisDraggedFrom,
                    DomEl.prototype._whenOtherDraggedOverThis,
                    DomEl.prototype._whenOtherDraggedOffThis,
                    DomEl.prototype._whenOtherDraggedToThis
                ];
                
                /* Act. */
                // Method under test is called by the constructor.
                let target = new DomEl(null, []);
                
                /* Assert. */
                for (let at of events) {
                    // Localizing.
                    let expected = expecteds[at];
                    let event = events[at];
                    let actual = target._eventListeners.get(event);
                    
                    // Comparing.
                    expect(actual).to.equal(expected);
                }
            });
        });

        it("_addSelfToStorage() is called.", /* working */ () => {
            /* Arrange. */
            // Temporary method redefinition to set an indicator.
            let wasAddedToStorage = false;
            let real_addSelfToStorage = DomEl.prototype._addSelfToStorage;

            DomEl.prototype._addSelfToStorage = () => {
                wasAddedToStorage = true;
            }

            /* Act. */
            let target = new DomEl(null, []);

            /* Aftermath. */
            DomEl.prototype._addSelfToStorage = real_addSelfToStorage;

            /* Assert. */
            expect(wasAddedToStorage).to.be.true;
        });
        
        describe("_initDomNode() and its operations", () => {
            it("_initDomNode() is called.", /* working */ () => {
                /* Arrange. */
                let real_wireSelfEventListeners = DomEl.prototype._wireSelfEventListeners;
                DomEl.prototype._wireSelfEventListeners = () => { };

                // Temporary method redefinition to set a test sign.
                let domNodeWasInited = false;

                let real_initDomNode = DomEl.prototype._initDomNode;

                DomEl.prototype._initDomNode = () => {
                    domNodeWasInited = true;
                }

                /* Act. */
                let target = new DomEl(null, []);

                /* Aftermath. */
                DomEl.prototype._initDomNode = real_initDomNode;
                DomEl.prototype._wireSelfEventListeners = real_wireSelfEventListeners;

                /* Assert. */
                expect(domNodeWasInited).to.be.true;
            });

            it("DomEl's .domNode is set to a deep clone of .stencil.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assemble. */
                let targetStencil = target.stencil;

                // These are set after the tested code.
                // These steps parallel _initDomNode().
                targetStencil.id = target.id;
                targetStencil.classList.add(target.class);

                /* Assert. */
                expect(target.domNode).to.deep.equal(targetStencil);
            });

            it(".domNode.id is set to the DomEl's .id.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assemble. */
                let targetIdAsText = String(target.id);

                /* Assert. */
                expect(target.domNode.id).to.equal(targetIdAsText);
            });

            it(".class is added to .domNode's .classList.", /* working */ () => {
                /* No arranging. */

                /* Act. */
                let target = new DomEl(null, []);

                /* Assert. */
                expect(target.domNode.classList.contains(target.class)).to.be.true;
            });

        });

        it("_setNodeSize() is called.", /* working */ () => {
            /* Arrange. */
            // Temporary method redefinition to set an indicator.
            let sizeWasSet = false;
            let real_setNodeSize = DomEl.prototype._setNodeSize;
            DomEl.prototype._setNodeSize = () => {
                sizeWasSet = true;
            };

            /* Act. */
            let target = new DomEl(null, []);

            /* Aftermath. */
            // Restoring the normal definition for other tests.
            DomEl.prototype._setNodeSize = real_setNodeSize;

            /* Assert. */
            expect(sizeWasSet).to.be.true;
        });

        it("_wireSelfEventListeners() is called.", /* working */ () => {
            /* Arrange. */
            // Temporary method redefinition to set an indicator.
            let listenersWereWired = false;
            let real_wireSelfEventListeners = DomEl.prototype._wireSelfEventListeners;

            DomEl.prototype._wireSelfEventListeners = () => {
                listenersWereWired = true;
            };

            /* Act. */
            let target = new DomEl(null, []);

            /* Aftermath. */
            // Restoring the normal definition for other tests.
            DomEl.prototype._wireSelfEventListeners = real_wireSelfEventListeners;

            /* Assert. */
            expect(listenersWereWired).to.be.true;
        });

    });

    describe("_wireSelfEventListeners()", () => {
        it("Each of the drag and drop events has the right listener wired to it.", /* working */ () => {
            /* Arrange. */
            // Before being temporarily redefined for test signs.
            let real_whenDraggedFrom = DomEl.prototype._whenThisDraggedFrom;
            let real_whenDraggedOver = DomEl.prototype._whenOtherDraggedOverThis;
            let real_whenDraggedOff = DomEl.prototype._whenOtherDraggedOffThis;
            let real_whenDraggedTo = DomEl.prototype._whenOtherDraggedToThis;

            // Test signs.
            DomEl.prototype._whenThisDraggedFrom = function() { this.wasFrom = true; };
            DomEl.prototype._whenOtherDraggedOverThis = function() { this.wasOver = true; };
            DomEl.prototype._whenOtherDraggedOffThis = function() { this.wasOff = true; };
            DomEl.prototype._whenOtherDraggedToThis = function() { this.wasTo = true; };
            
            /* Act. */
            // The method under test is 
            // called by the constructor.
            let target = new DomEl();
            
            /* Assemble. */
            // Each event is raised in turn, 
            // so the listeners set the signs.
            target.domNode.dispatchEvent(new MouseEvent("dragstart"));
            target.domNode.dispatchEvent(new MouseEvent("dragover"));
            target.domNode.dispatchEvent(new MouseEvent("dragleave"));
            target.domNode.dispatchEvent(new MouseEvent("drop"));
            
            /* Aftermath. */
            DomEl.prototype._whenThisDraggedFrom = real_whenDraggedFrom;
            DomEl.prototype._whenOtherDraggedOverThis = real_whenDraggedOver;
            DomEl.prototype._whenOtherDraggedOffThis = real_whenDraggedOff;
            DomEl.prototype._whenOtherDraggedToThis = real_whenDraggedTo;
            
            /* Assert. */
            // If listeners never called, 
            // these would be undefined.
            expect(target.wasFrom).to.be.true;
            expect(target.wasOver).to.be.true;
            expect(target.wasOff).to.be.true;
            expect(target.wasTo).to.be.true;
        });

        it("`this` in a wired listener is its originating DomEL object.", /* working */ () => {
            /* Throws if `this` is not the DomEl, and in 
               the process, fails to set the indicator. */

            /* Arrange. */
            let target = new DomEl(null, []);

            // The node for the DomEl, which the DOM event is raised on.
            let node = document.createElement("DIV");
            node.id = "dom-node";
            target._domNode = node;

            let event = new Event("dragstart");

            /* Act. */
            target._wireSelfEventListeners();

            /* Assemble. */
            node.dispatchEvent(event);

            // If `this` is `target`, this is defined.
            let actual = target._dnd.dragged;

            /* Assert.*/
            expect(actual).to.equal(target);
        });

    });

    describe("destructor()", () => {
        it("destructor() is called on each DomEl in .children.", /* working */ () => {
            /* Arrange. */
            // Test signs.            
            let children = DomElFixtures.createMinimalChildren(3);
            let actuals = [];
            
            for (let child of children) {
                child.destructor = () => {
                    actuals.push(true);
                };
            }
            
            let target = new DomEl(null, children);
            
            /* Act. */
            target.destructor();
            
            /* Assert. */
            let expecteds = [ true, true, true ];
            expect(actuals).to.deep.equal(expecteds);
        });
        
        it(".children is changed to an empty array.", /* working */ () => {
            /* Arrange. */
            let children = DomElFixtures.createMinimalChildren(3);
            let target = new DomEl(null, children);
            
            /* Ascertain. */
            expect(target.children).to.deep.equal(children);

            /* Act. */
            target.destructor();

            /* Assert. */
            let empty = [];
            expect(target.children).to.be.empty;
        });
        
        it(".parent is set to null.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl();
            let target = new DomEl(parent);
            
            /* Ascertain. */
            expect(target.parent).to.equal(parent);
            
            /* Act. */
            target.destructor();
            
            /* Assert. */
            expect(target.parent).to.be.null;
        });
        
        it(".isDisplayed is set to false.", /* working */ () => {
            /* Arrange. */
            let parent = new DomEl();
            let target = new DomEl(parent);
            
            // Test sign.
            target.isDisplayed = true;

            /* Act. */
            target.destructor();

            /* Assert. */
            expect(target.isDisplayed).to.be.false;
        });
        
        it("_unwireSelfEventListeners() is called.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl();
            
            // Test sign.
            let wasCalled = false;
            target._unwireSelfEventListeners = () => {
                wasCalled = true;
            };
            
            /* Act. */
            target.destructor();
            
            /* Assert. */
            expect(wasCalled).to.be.true;
        });
        
        it("_removeSelfFromStorage() is called.", /* working */ () => {
            /* Arrange. */
            let target = new DomEl();

            // Test sign.
            let wasCalled = false;
            target._removeSelfFromStorage = () => {
                wasCalled = true;
            };

            /* Act. */
            target.destructor();

            /* Assert. */
            expect(wasCalled).to.be.true;
        });
        
    });

    describe("_unwireSelfEventListeners()", () => {
        it("None of the wired drag and drop events has a listener any longer.", /* working */ () => {
            /* Arrange. */
            // Before being temporarily redefined for test signs.
            let real_whenDraggedFrom = DomEl.prototype._whenThisDraggedFrom;
            let real_whenDraggedOver = DomEl.prototype._whenOtherDraggedOverThis;
            let real_whenDraggedOff = DomEl.prototype._whenOtherDraggedOffThis;
            let real_whenDraggedTo = DomEl.prototype._whenOtherDraggedToThis;

            // Test signs.  If these aren't called, the properties are left undefined.
            DomEl.prototype._whenThisDraggedFrom = function() { this.wasFrom = true; };
            DomEl.prototype._whenOtherDraggedOverThis = function() { this.wasOver = true; };
            DomEl.prototype._whenOtherDraggedOffThis = function() { this.wasOff = true; };
            DomEl.prototype._whenOtherDraggedToThis = function() { this.wasTo = true; };

            // The listeners are wired by a dependency of the constructor.
            let target = new DomEl();

            /* Act. */
            target._unwireSelfEventListeners();

            /* Assemble. */
            // Each event is raised in turn, 
            // so the listeners set the signs.
            target.domNode.dispatchEvent(new MouseEvent("dragstart"));
            target.domNode.dispatchEvent(new MouseEvent("dragover"));
            target.domNode.dispatchEvent(new MouseEvent("dragleave"));
            target.domNode.dispatchEvent(new MouseEvent("drop"));

            /* Aftermath. */
            DomEl.prototype._whenThisDraggedFrom = real_whenDraggedFrom;
            DomEl.prototype._whenOtherDraggedOverThis = real_whenDraggedOver;
            DomEl.prototype._whenOtherDraggedOffThis = real_whenDraggedOff;
            DomEl.prototype._whenOtherDraggedToThis = real_whenDraggedTo;

            /* Assert. */
            expect(target.wasFrom).to.be.undefined;
            expect(target.wasOver).to.be.undefined;
            expect(target.wasOff).to.be.undefined;
            expect(target.wasTo).to.be.undefined;
        });
    });
});

