/**/

import { DomEl } from "../../DomEl.js";

// region fixtures for tests of DomEl

export class DomElFixtures {
    static createMinimalChildren(count) /* verified */ {
        let children = [];

        for (let of = 0; of < count; of++) {
            let child = this.createMinimalChild();
            children.push(child);
        }

        return children;
    }

    static createMinimalChild() /* verified */ {
        // Ensuring tests work with real DomEl children.
        // Exposes and sets many indicators.
        let child = new DomEl("parent", []);
        return child;
    }

    static createMinimalSpoofEventArg(targetName) /* verified */ {
        // Indicators to see if the (spoof) object is used right.
        let arg = {
            stopWasCalled: false,
            defaultWasPrevented: false,
            target: targetName,
            dataTransfer: { dropEffect: "not-set", effectAllowed: "not-set" }
        };

        // Spoof methods that set indicators.
        arg.stopPropagation = function () {
            this.stopWasCalled = true;
        };
        arg.preventDefault = function () {
            this.defaultWasPrevented = true;
        };

        // Ensuring spoof methods can set 
        // indicators on their object.
        arg.stopPropagation.bind(arg);
        arg.preventDefault.bind(arg);

        return arg;
    }

    static createSimpleDomSubtree() /* verified */ {
        let root = document.createElement("DIV");

        let h1 = document.createElement("H1");
        h1.innerText = "h1 text";

        let p = document.createElement("P");
        p.innerText = "p text";

        root.appendChild(h1);
        root.appendChild(p);

        return root;
    }

    static addChildrenToDomEl(target, children) /* verified */ {
        for (let child of children) {
            target.addChild(child);
        }
    }

    static getTopOfDomNode(domEl) /* verified */ {
        let domNode = domEl.domNode;

        let box = domNode.getBoundingClientRect();
        let top = box.top;

        return top;
    }
    
    static spoofScreenLocations(domEls) /* verified */ {
        for (let domEl of domEls) {
            DomElFixtures.setSpoofGetBoundingClientRect(domEl);
        }
    }
    
    static setSpoofGetBoundingClientRect(domEl) /* verified */ {
        domEl.domNode.getBoundingClientRect = () => {
            return { top: domEl.id * 10 };
        }
    }

}

// endregion fixtures for tests of DomEl

