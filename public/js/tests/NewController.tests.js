/**/

import { expect } from "@esm-bundle/chai";

import { NewController } from "../NewController.js";
import { Storage } from "../Storage.js";
import { BlurbSet } from "../BlurbSet.js";
import { Blurb } from "../Blurb.js";

function createBlurbSets(setCount, blurbCount) /* verified */ {
    let blurbSets = [];

    for (let at = 0; at < setCount; at++) {
        let blurbs = [];

        for (let of = 0; of < blurbCount; of++) {
            blurbs.push(new Blurb(`${ at }-${ of }`, 0, 1, 2, 3));
        }

        let blurbSet = new BlurbSet(`${ at }`, blurbs, at, false);
        blurbSets.push(blurbSet);
    }

    return blurbSets;
}

function createSpoofStorage(blurbSets) /* verified */ {
    let spoof = new Storage();
    spoof.length = blurbSets.length;

    spoof.contents = new Map();
    let contents = spoof.contents;

    let names = [];

    for (let blurbSet of blurbSets) {
        names.push(blurbSet.name);
        contents.set(blurbSet.name, JSON.stringify(blurbSet));
    }

    contents.set("BlurbSetNames", JSON.stringify(names));

    spoof.key = function(at) {
        let keys = this.contents.keys();
        return keys[at];
    }

    spoof.getItem = function(key) {
        return this.contents.get(key);
    }

    spoof.setItem = function(key, value) {
        this.contents.set(key, value);
    }

    spoof.removeItem = function(key) {
        this.contents.delete(key);
    }
    
    spoof.key.bind(spoof);
    spoof.getItem.bind(spoof);
    spoof.setItem.bind(spoof);
    spoof.removeItem.bind(spoof);
    
    return spoof;
}


describe("NewController", () => {
    describe("retrieveAnyBlurbContent()", () => {
        it("All blurb groups / sets stored on .storage are added to .blurbSets.", /* working */ () => {
            /* Arrange. */
            let target = new NewController(document);
            
            let blurbSets = createBlurbSets(7, 3);
            let spoofStorage = createSpoofStorage(blurbSets);
            
            target.storage = spoofStorage;
            
            /* Act. */
            target.retrieveAnyBlurbContent();
            
            /* Assert. */
            expect(target.blurbSets).to.deep.equal(blurbSets);
        });
    });
});
