/**/

import { expect } from "@esm-bundle/chai";

import { Blurb } from "../blurb.js";

describe("Blurb", () => {
    // region Static level properties

    it("Static .h1 returns \"h1\".", /* working */ () => {
        /* Arrange. */
        let expected = "h1";

        /* Act. */
        let actual = Blurb.h1;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .h2 returns \"h2\".", /* working */ () => {
        /* Arrange. */
        let expected = "h2";

        /* Act. */
        let actual = Blurb.h2;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .h3 returns \"h3\".", /* working */ () => {
        /* Arrange. */
        let expected = "h3";

        /* Act. */
        let actual = Blurb.h3;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .good returns \"good\".", /* working */ () => {
        /* Arrange. */
        let expected = "good";

        /* Act. */
        let actual = Blurb.good;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .bad returns \"bad\".", /* working */ () => {
        /* Arrange. */
        let expected = "bad";

        /* Act. */
        let actual = Blurb.bad;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .neutral returns \"neutral\".", /* working */ () => {
        /* Arrange. */
        let expected = "neutral";

        /* Act. */
        let actual = Blurb.neutral;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    it("Static .plain returns \"plain\".", /* working */ () => {
        /* Arrange. */
        let expected = "plain";

        /* Act. */
        let actual = Blurb.plain;

        /* Assert. */
        expect(actual).to.equal(expected);
    });

    // endregion Static level properties

    // region Constructor

    // endregion Constructor

    // region Static fromJson()

    // endregion Static fromJson()
});
