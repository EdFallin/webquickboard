/**/

import { expect } from "@esm-bundle/chai";

import { BlurbEl } from "../BlurbEl.js";
import { Blurb } from "../Blurb.js";
import { BlurbSet } from "../BlurbSet.js";
import { GroupBlurbsEl } from "../GroupBlurbsEl.js";
import { BlurbsRootEl } from "../BlurbsRootEl.js";

// region Fixtures and fixture dependencies

let realTypes = new Map();

function swapToRealMethods(type, ...methods) /* verified */ {
    realTypes.set(type, new Map());

    let realMethods = realTypes.get(type);

    for (let method of methods) {
        realMethods.set(method.name, method);
    }
}

function swapInEmptyMethods(type, ...methods) /* verified */ {
    for (let method of methods) {
        type.prototype[method.name] = () => { };
    }
}

function swapFromRealMethods(type) /* verified */ {
    let realMethods = realTypes.get(type);
    let keys = realMethods.keys();

    for (let key of keys) {
        let realMethod = realMethods.get(key);
        type.prototype[key] = realMethod;
    }
}

// endregion Fixtures and fixture dependencies

describe("BlurbEl", () => {
    // region canDropThisOnto()

    it("When BlurbEl is dropped onto another BlurbEl, canDropThisOnto() returns true.", /* working */ () => {
        /* Arrange. */
        let blurb = new Blurb("blurb-text");

        let type = BlurbEl.prototype;
        
        // Injecting empty internal dependencies.
        swapToRealMethods(BlurbEl, type._initDomNode, type._wireSelfEventListeners);
        swapInEmptyMethods(BlurbEl, type._initDomNode, type._wireSelfEventListeners);

        let target = new BlurbEl(blurb);
        let onto = new BlurbEl(blurb);

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        // Reverting to real internal dependencies.
        swapFromRealMethods(BlurbEl);

        /* Assert. */
        expect(actual).to.be.true;
    });

    it("When BlurbEl is dropped onto GroupBlurbsEl, canDropThisOnto() returns true.", /* working */ () => {
        /* Arrange. */
        let blurb = new Blurb("blurb-text");
        let blurbSet = new BlurbSet("blurbs-name", [ blurb ]);
        
        // Injecting empty internal dependencies.
        let single = BlurbEl.prototype;
        swapToRealMethods(BlurbEl, single._initDomNode, single._wireSelfEventListeners);
        swapInEmptyMethods(BlurbEl, single._initDomNode, single._wireSelfEventListeners);

        let group = GroupBlurbsEl.prototype;
        swapToRealMethods(GroupBlurbsEl, group._initDomNode, group._wireSelfEventListeners, group._initEmptyBlurb);
        swapInEmptyMethods(GroupBlurbsEl, group._initDomNode, group._wireSelfEventListeners, group._initEmptyBlurb);

        let target = new BlurbEl(blurb);
        let onto = new GroupBlurbsEl(blurbSet);

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        // Reverting to real internal dependencies.
        swapFromRealMethods(BlurbEl);
        swapFromRealMethods(GroupBlurbsEl);

        /* Assert. */
        expect(actual).to.be.true;
    });

    it("When BlurbEl is dropped onto some other DomEl, canDropThisOnto() returns false.", /* working */ () => {
        /* Arrange. */
        let blurb = new Blurb("blurb-text");
        
        // Injecting empty internal dependencies.
        let single = BlurbEl.prototype;
        swapToRealMethods(BlurbEl, single._initDomNode, single._wireSelfEventListeners);
        swapInEmptyMethods(BlurbEl, single._initDomNode, single._wireSelfEventListeners);

        // More injecting, with a plausible not-droppable DomEl.
        let root = BlurbsRootEl.prototype;
        swapToRealMethods(BlurbsRootEl, root._initDomNode, root._wireSelfEventListeners);
        swapInEmptyMethods(BlurbsRootEl, root._initDomNode, root._wireSelfEventListeners);

        let target = new BlurbEl(blurb);
        let onto = new BlurbsRootEl();

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        // Reverting to real internal dependencies.
        swapFromRealMethods(BlurbEl);
        swapFromRealMethods(BlurbsRootEl);

        /* Assert. */
        expect(actual).to.be.false;
    });

    it("When BlurbEl is dropped onto a non-DomEl DOM node, canDropThisOnto() returns false.", /* working */ () => {
        /* Arrange. */
        let blurb = new Blurb("blurb-text");
        
        // Injecting empty internal dependencies.
        let type = BlurbEl.prototype;
        swapToRealMethods(BlurbEl, type._initDomNode, type._wireSelfEventListeners);
        swapInEmptyMethods(BlurbEl, type._initDomNode, type._wireSelfEventListeners);

        let target = new BlurbEl(blurb);

        // Test condition: Not a DomEl.
        let onto = document.createElement("DIV");

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        // Reverting to real internal dependencies.
        swapFromRealMethods(BlurbEl);

        /* Assert. */
        expect(actual).to.be.false;
    });

    // endregion canDropThisOnto()
});
