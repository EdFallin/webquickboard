/**/

import { expect } from "@esm-bundle/chai";

import { Dom } from "../Dom.js";

describe("Dom", () => {
    describe(".singleton", () => {
        it("When .singleton is called when not set (null or undefined), a new Dom is created for it.", /* working */ () => {
            /* Arrange. */
            // Setting field to not-there, because other tests might set it.
            Dom._singleton = undefined;

            /* Act. */
            let actual = Dom.singleton;

            /* Assert. */
            expect(actual).not.to.be.null;
            expect(actual.doc).to.equal(document);
        });

        it("When .singleton is set to a DOM instance, that instance is used afterward.", /* working */ () => {
            /* Arrange. */
            let notExpected = Dom.singleton;
            let expected = new Dom(document);

            /* Act. */
            Dom.singleton = expected;

            /* Assert. */
            let actual = Dom.singleton;

            expect(actual).to.equal(expected);
            expect(actual).not.to.equal(notExpected);
        });

    });

    describe(".nextId", () => {
        it("Each call returns the next sequential number, even when called on .singleton.", /* working */ () => {
            /* Arrange. */
            let expecteds = [ 1, 2, 3, 4 ];
            let actuals = [];

            // Setting the Singleton to ensure that 
            // this test is always fully isolated.
            Dom.singleton = new Dom(document);

            /* Act. */
            for (let at = 0; at < 4; at++) {
                let passer = Dom.singleton.nextId;
                actuals.push(passer);
            }

            /* Assert. */
            expect(actuals).to.deep.equal(expecteds);
        });
    });

    describe("constructor()", () => {
        it("A new Dom sets all its node properties to the matching DOM nodes.", /* working */ () => {
            /* Arrange. */
            let ids = [
                "GroupElStencil", "GroupBlurbsElStencil",
                "BlurbElStencil", "EmptyBlurbGroupElStencil",
                "EmptyBlurbElStencil", "BlurbSets", "Blurbs"
            ];

            let expecteds = [];

            // The default document in the test context already 
            // is an HTML document, with a head and body.
            let root = document.querySelector("body");
            
            // Removing any old instances of targeted nodes to isolate 
            // this test, since `document` is global to all tests.
            let nodes = [...root.childNodes];
            
            for (let node of nodes) {
                if (ids.includes(node.id)) {
                    root.removeChild(node);
                }
            }

            // Adding the DOM elements set on Dom.
            for (let id of ids) {
                let node = document.createElement("DIV");
                node.id = id;
                root.appendChild(node);

                expecteds.push(node);
            }

            // Ensuring the Dom instance is a fresh 
            // one using the current ``document.
            Dom._singleton = null;

            /* Act. */
            // .singleton calls constructor() internally.
            let actual = Dom.singleton;

            /* Assert. */
            expect(actual.groupElStencil).to.equal(expecteds[0]);
            expect(actual.groupBlurbsElStencil).to.equal(expecteds[1]);
            expect(actual.blurbElStencil).to.equal(expecteds[2]);

            expect(actual.emptyBlurbGroupElStencil).to.equal(expecteds[3]);
            expect(actual.emptyBlurbElStencil).to.equal(expecteds[4]);

            expect(actual.blurbSets).to.equal(expecteds[5]);
            expect(actual.blurbs).to.equal(expecteds[6]);
        });
    });
});
