/**/

import { expect } from "@esm-bundle/chai";

import { NewDom } from "../NewDom.js";

describe("NewDom", () => {
    describe(".singleton", () => {
        it("When .singleton is called when not set (null or undefined), a new NewDom is created for it.", /* working */ () => {
            /* Arrange. */
            // Setting field to not-there, because other tests might set it.
            NewDom._singleton = undefined;

            /* Act. */
            let actual = NewDom.singleton;

            /* Assert. */
            expect(actual).not.to.be.null;
            expect(actual.doc).to.equal(document);
        });

        it("When .singleton is set to a DOM instance, that instance is used afterward.", /* working */ () => {
            /* Arrange. */
            let notExpected = NewDom.singleton;
            let expected = new NewDom(document);

            /* Act. */
            NewDom.singleton = expected;

            /* Assert. */
            let actual = NewDom.singleton;

            expect(actual).to.equal(expected);
            expect(actual).to.not.equal(notExpected);
        });

    });
    
    describe(".nextId", () => {
        it("Each call returns the next sequential number, even when called on .singleton.", /* working */ () => {
            /* Arrange. */
            let expecteds = [ 1, 2, 3, 4 ];
            let actuals = [];

            // Setting the Singleton to ensure that 
            // this test is always fully isolated.
            NewDom.singleton = new NewDom(document);

            /* Act. */
            for (let at = 0; at < 4; at++) {
                let passer = NewDom.singleton.nextId;
                actuals.push(passer);
            }

            /* Assert. */
            expect(actuals).to.deep.equal(expecteds);
        });
    });

    describe("constructor()", () => {
        it("A new NewDom sets all its node properties to the matching DOM nodes.", /* working */ () => {
            /* Arrange. */
            let ids = [
                "GroupsRoot", 
                "BlurbsRoot",
                "EmptyGroupElStencil",
                "GroupBlurbsElStencil",
                "GroupElStencil", 
                "BlurbElStencil", 
                "EmptyBlurbElStencil", 
            ];

            let expecteds = [];

            // The default document in the test context already 
            // is an HTML document, with a head and body.
            let root = document.querySelector("body");

            // Removing any old instances of targeted nodes to isolate 
            // this test, since `document` is global to all tests.
            let nodes = [...root.childNodes];

            for (let node of nodes) {
                if (ids.includes(node.id)) {
                    root.removeChild(node);
                }
            }
            
            // Adding the DOM elements set on NewDom.
            for (let id of ids) {
                let node = document.createElement("DIV");
                node.id = id;
                root.appendChild(node);

                expecteds.push(node);
            }

            // Ensuring the NewDom instance is a fresh 
            // one using the current ``document.
            NewDom._singleton = null;

            /* Act. */
            // .singleton calls constructor() internally.
            let actual = NewDom.singleton;

            /* Assert. */
            expect(actual.groupsRoot).to.equal(expecteds[0]);
            expect(actual.blurbsRoot).to.equal(expecteds[1]);

            expect(actual.emptyGroupElStencil).to.equal(expecteds[2]);
            expect(actual.groupBlurbsElStencil).to.equal(expecteds[3]);
            expect(actual.groupElStencil).to.equal(expecteds[4]);

            expect(actual.blurbElStencil).to.equal(expecteds[5]);
            expect(actual.emptyBlurbElStencil).to.equal(expecteds[6]);
        });
    });
});
