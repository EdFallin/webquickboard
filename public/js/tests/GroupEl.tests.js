/**/

import { expect } from "@esm-bundle/chai";

import { GroupEl } from "../GroupEl.js";
import { BlurbSet } from "../BlurbSet.js";
import { GroupsRootEl } from "../GroupsRootEl.js";
import { BlurbsRootEl } from "../BlurbsRootEl.js";

// region Fixtures and fixture dependencies

let realTypes = new Map();

function swapToRealMethods(type, ...methods) /* verified */ {
    realTypes.set(type, new Map());

    let realMethods = realTypes.get(type);

    for (let method of methods) {
        realMethods.set(method.name, method);
    }
}

function swapInEmptyMethods(type, ...methods) /* verified */ {
    for (let method of methods) {
        type.prototype[method.name] = () => { };
    }
}

function swapFromRealMethods(type) /* verified */ {
    let realMethods = realTypes.get(type);
    let keys = realMethods.keys();

    for (let key of keys) {
        let realMethod = realMethods.get(key);
        type.prototype[key] = realMethod;
    }
}

// endregion Fixtures and dependencies

describe("GroupEl", () => {
    // region canDropThisOnto()

    it("When GroupEl is dropped onto another GroupEl, canDropThisOnto() returns true.", /* working */ () => {
        /* Arrange. */
        let blurbs = new BlurbSet("blurb-set-name");

        // Injecting empty internal dependencies.
        let type = GroupEl.prototype;
        swapToRealMethods(GroupEl, type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);
        swapInEmptyMethods(GroupEl, type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);

        let target = new GroupEl(blurbs);
        let onto = new GroupEl(blurbs);

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        // Reverting to real internal dependencies.
        swapFromRealMethods(GroupEl);

        /* Assert. */
        expect(actual).to.be.true;
    });

    it("When GroupEl is dropped onto a GroupsRootEl, canDropThisOnto() returns true.", /* working */ () => {
        /* Arrange. */
        let blurbs = new BlurbSet("blurb-set-name");

        // Injecting empty internal dependencies.
        let group = GroupEl.prototype;
        swapToRealMethods(group._initDomNode, group._wireSelfEventListeners, group._initDisplayer);
        swapInEmptyMethods(GroupEl, group._initDomNode, group._wireSelfEventListeners, group._initDisplayer);

        let root = GroupsRootEl.prototype;
        swapToRealMethods(GroupsRootEl, root._wireSelfEventListeners, root._initSubtreeMembers);
        swapInEmptyMethods(GroupsRootEl, root._wireSelfEventListeners, root._initSubtreeMembers);

        let target = new GroupEl(blurbs);
        let onto = new GroupsRootEl();

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        swapFromRealMethods(GroupEl);
        swapFromRealMethods(GroupsRootEl);

        /* Assert. */
        expect(actual).to.be.true;
    });

    it("When GroupEl is dropped onto some other DomEl, canDropThisOnto() returns false.", /* working */ () => {
        /* Arrange. */
        let blurbs = new BlurbSet("blurb-set-name");

        // Injecting empty internal dependencies.
        let type = GroupEl.prototype;
        swapToRealMethods(type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);
        swapInEmptyMethods(GroupEl, type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);

        // More injecting, with a plausible not-droppable DomEl.
        let root = BlurbsRootEl.prototype;
        swapToRealMethods(BlurbsRootEl, root._wireSelfEventListeners);
        swapInEmptyMethods(BlurbsRootEl, root._wireSelfEventListeners);

        let target = new GroupEl(blurbs);
        let onto = new BlurbsRootEl();

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        swapFromRealMethods(GroupEl);
        swapFromRealMethods(BlurbsRootEl);

        /* Assert. */
        expect(actual).to.be.false;
    });

    it("When GroupEl is dropped onto a non-DomEl DOM node, canDropThisOnto() returns false.", /* working */ () => {
        /* Arrange. */
        let blurbs = new BlurbSet("blurb-set-name");

        // Injecting empty internal dependencies.
        let type = GroupEl.prototype;
        swapToRealMethods(type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);
        swapInEmptyMethods(GroupEl, type._initDomNode, type._wireSelfEventListeners, type._initDisplayer);

        let target = new GroupEl(blurbs);

        // Test condition: Not a DomEl.
        let onto = document.createElement("DIV");

        /* Act. */
        let actual = target.canDropThisOnto(onto);

        /* Afterward. */
        swapFromRealMethods(GroupEl);

        /* Assert. */
        expect(actual).to.be.false;
    });

    // endregion canDropThisOnto()

});
