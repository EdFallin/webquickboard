/**/

import { NewDom } from "./NewDom.js";
import { DragAndDrop } from "./DragAndDrop.js";

/* This class is the semi-abstract Composite node class.  
   Subclasses can override / reinvoke instance members freely.  
   The statics are different for each subclass. */
export class DomEl {
    // region properties

    get id() {
        return this._id;
    }

    get isDisplayed() {
        return this._isDisplayed;
    }

    set isDisplayed(value) {
        this._isDisplayed = value;
    }

    /* Overridden by subclasses. */
    get class() {
        return "DomEl";
    }

    /* The DOM element for this instance. */
    get domNode() {
        return this._domNode;
    }
    
    set domNode(value) {
        this._domNode = value;
    }

    /* The JS objects related to this instance. */
    get children() {
        return this._children;
    }

    get parent() {
        return this._parent;
    }

    set parent(item) {
        this._parent = item;
    }

    /* Overridden by subclasses. */
    get stencil() {
        /* A default trivial DOM node. */
        let stencil = document.createElement("DIV");
        stencil.innerText = "Default DOM node.  Subclasses should override.";

        return stencil;
    }

    // endregion properties

    // region constructor() and dependencies

    /* Construction of a DomEl can include these, but doesn't have to: 
     *        Parent:  The parent DomEl object for this object.
     *      Children:  Any initial DomEl child objects for this object.  
     *                 These are not the els in the DOM, 
     *                 but the JS objects that retain them.
     *  */
    constructor(parent, children = []) /* passed */ {
        /* Key operations are factored so subclasses 
           can redefine them however they need. */

        this._setBasicInstanceMembers();
        
        // DOM node, then its events for Composite.
        this._initDomNode();
        this._defineEventListeners();
        this._wireSelfEventListeners();

        // Composite.
        this._linkParentToSelf(parent);
        this._linkChildrenToSelf(children);

        /* &cruft, not sure if this should be here */
        this._addSelfToStorage();
    }

    _setBasicInstanceMembers() /* verified */ {
        this._id = NewDom.singleton.nextId;
        this._parent = null;
        this._isDisplayed = false;
        this._children = [];
        this._dnd = DragAndDrop.singleton;
        this._dom = NewDom.singleton;
    }

    _linkParentToSelf(parent) /* verified */ {
        if (parent instanceof DomEl) {
            parent.addChild(this);
        }
    }

    _linkChildrenToSelf(children) /* verified */ {
        for (let child of children) {
            this.addChild(child);
        }
    }

    _defineEventListeners() /* verified */ {
        /* Each listener's new bound identity is retained 
         * for adding and for removing easily later on. */

        this._eventListeners = new Map();

        this._eventListeners.set(
            "dragstart",
            this._whenThisDraggedFrom.bind(this)
        );

        this._eventListeners.set(
            "dragover",
            this._whenOtherDraggedOverThis.bind(this)
        );

        this._eventListeners.set(
            "dragleave",
            this._whenOtherDraggedOffThis.bind(this)
        );

        this._eventListeners.set(
            "drop",
            this._whenOtherDraggedToThis.bind(this)
        );
    }

    _addSelfToStorage() {
    }

    /* Overridden by some subclasses. */
    _initDomNode() /* verified */ {
        // The DOM node itself.
        let clone = this.stencil.cloneNode(true);
        this._domNode = clone;

        // Node properties for Composite.
        this._domNode.id = this._id;
        this._domNode.classList.add(this.class);

        // Node's (eventual) appearance. 
        this._setNodeSize();
    }

    _setNodeSize() {
        /* &cruft, size probably needs to be set when displaying */
    }

    _wireSelfEventListeners() /* passed */ {
        for (let key of this._eventListeners.keys()) {
            let value = this._eventListeners.get(key);
            this.domNode.addEventListener(key, value);
        }
    }

    // endregion constructor() and dependencies

    // region display() and dependencies

    display() /* passed */ {
        this._isDisplayed = true;

        // Composite.  Preorder depth-first recursion.
        this._displaySelf();
        this._displayChildren();
    }

    _displaySelf() /* passed */ {
        let at = this.parent.children.indexOf(this);

        let parentDomNode = this.parent.domNode;
        let before = parentDomNode.children[at];
        
        parentDomNode.insertBefore(this.domNode, before);
    }

    _displayChildren() {
        // Displaying current child DOM nodes.
        for (let child of this._children) {
            child.display();
        }
    }

    // endregion display() and dependencies

    // region drag and drop

    _whenThisDraggedFrom(e) /* passed */ {
        /* Raised when this DomEl's .domNode is dragged. */

        // Dnd works only once at a time.
        e.stopPropagation();

        // DomEls needed throughout dnd.
        this._dnd.dragged = this;
    }

    _whenOtherDraggedOverThis(e) /* passed */ {
        /* Raised when this DomEl's .domNode is dragged over. */

        /* &cruft, if I want to have orange bars between items, 
           then this method also adds the CSS class for the bar */

        e.preventDefault();

        let crossed = this;
        let dragged = this._dnd.dragged;

        if (!dragged.canDropThisOnto(crossed)) {
            e.dataTransfer.dropEffect = "none";
            e.dataTransfer.effectAllowed = "none";

            /* Event propagation is not stopped, in case 
               an ancestor DomEl should respond to it.*/
            return;
        }

        // Without shift or alt, drag is 
        // a move; with it pressed, a copy.
        let effect = e.shiftKey || e.altKey ? "copy" : "move";

        e.dataTransfer.dropEffect = effect;
        e.dataTransfer.effectAllowed = effect;

        /* &cruft, what else belongs here? */
        e.stopPropagation();
    }

    _whenOtherDraggedOffThis(e) /* ok */ {
        /* &cruft, if I want to have orange bars between items, 
           then this method removes the CSS class for the bar */
    }

    _whenOtherDraggedToThis(e) /* passed */ {
        /* Raised when this DomEl's .domNode is dragged onto. */

        // Always needed here.
        e.preventDefault();

        let onto = this;
        let dragged = this._dnd.dragged;

        // Adding the dropped el / DomEl as defined by _whenDroppedOntoThis().
        // Chain of Responsibility: If not handled here, event propagates 
        // rootward for .parent of the next-rootward el to address.
        if (dragged.canDropThisOnto(onto)) {
            this._whenDroppedOntoThis(e);

            // No further drag and drop at ancestors.
            e.stopPropagation();
        }
    }

    canDropThisOnto(domEl) /* ok */ {
        /* Invoked when this DomEl's .domNode is the one being dragged; 
           this DomEl knows what DomEl subclasses it can be dropped on. */

        /* &cruft, implement this or just override it */
        return true;
    }

    /* Delegation point for drops onto this DomEl.  Within, can call one 
     * of the predefined delegation methods.  Drop operations must remove 
     * dragged DomEl / its DOM node from old and add to new. */
    _whenDroppedOntoThis(e) /* ok */ {
        /* Here, you can call one of the predefined 
         * methods, or custom-code one for the class. */
    }

    // region _whenDroppedOntoThis() delegate methods and dependencies

    _addAsChild(e) /* passed */ {
        // Original, or a clone.
        let dragged = this._calculateDragged(e);

        // Where to put dragged as a new child.
        let onto = this._calculateOntoChild(e);

        // Drag-onto-self makes no change.
        if (dragged === onto) {
            return;
        }

        // Removing and reindexing, if not cloned.
        this._removeDraggedFromParentIfMoving(e, dragged);

        // Actually adding as child.
        this.addChild(dragged, onto);
        
        // Needed for callers to call display().
        return dragged;
    }

    _addToParent(e) /* passed  */ {
        // Original, or a clone.
        let dragged = this._calculateDragged(e);

        // Drag-onto-self makes no change.
        if (dragged === this) {
            return this;
        }

        // Removing and reindexing, if not cloned.
        this._removeDraggedFromParentIfMoving(e, dragged);

        // Actually adding to parent.
        this.parent.addChild(dragged, this);

        // Needed for callers to call display().
        return dragged;
    }

    // region dependencies of _addAsChild() and _addToParent()

    _calculateDragged(e) /* passed */ {
        let dragged = this._dnd.dragged;

        // If control is pressed, copying, 
        // so making a clone to be dropped.
        if (e.shiftKey || e.altKey) {
            dragged = dragged.clone();
        }

        return dragged;
    }

    _calculateOntoChild(e) /* passed */ {
        // Default used if drop is after last child.
        let onto = null;

        // Finding any first child that the drop is above, 
        // using a numeric loop for onscreen DOM index.
        for (let at = 0; at < this.domNode.children.length; at++) {
            let domChild = this.domNode.children[at];

            let box = domChild.getBoundingClientRect();
            let top = box.top;

            if (e.clientY < top) {
                onto = this.children[at];
                break;
            }
        }

        return onto;
    }

    _removeDraggedFromParentIfMoving(e, dragged) /* passed */ {
        // If shift or alt, copying, not moving.
        if (e.shiftKey || e.altKey) {
            return;
        }

        // If control not pressed, moving, 
        // so dropping from tree and DOM.
        let from = dragged.parent;
        from.removeChild(dragged);
        dragged.domNode.remove();
    }

    // endregion dependencies of _addAsChild() and _addToParent()

    //endregion _whenDroppedOntoThis() delegate methods and dependencies

    // endregion drag and drop

    // region tree

    addChild(child, before) /* passed */ {
        let at = this._children.indexOf(before);

        // If no `before` (not provided or null), 
        // or not a child of this, to at the end.
        if (at === -1) {
            at = this._children.length;
        }

        // Add to the Composite.
        child.parent = this;
        this._children.splice(at, 0, child);
    }

    /* Removes a child but does not destroy it.  Used when moving a DomEl. */
    removeChild(child) /* passed */ {
        // Remove from the Composite.
        let at = this._children.indexOf(child);
        this._children.splice(at, 1);
        child._parent = null;

        // // Remove from the DOM.
        // child.domNode.remove();
    }

    /* Removes and destroys a child el.  Used when deleting a DomEL. */
    subtractChild(child) /* passed */ {
        // Remove from the Composite.
        let at = this._children.indexOf(child);
        this._children.splice(at, 1);

        // Remove from the DOM.
        child.domNode.remove();

        // The child is destructed after removal for simplicity.
        child.destructor();
    }

    /* Clones instance and either its child objects or its simple 
       HTML subtree, along with event listeners it already wires.
       Child classes may need to override this method in some cases. */
    clone() /* passed */ {
        // Cloning with .constructor() matches classes exactly.
        // Only clone's .id and .domNode.id are different.
        let cloned = new this.constructor();

        // When a leaf, address HTML.
        if (this.children.length == 0) {
            this._cloneLeafHtml(cloned);
        }

        // When a fork, address descendents.
        if (this.children.length > 0) {
            this._cloneDescendents(cloned);
        }

        return cloned;
    }

    _cloneLeafHtml(cloned) /* verified */ {
        /* If no children, the clone is a leaf, so this DomEl's HTML should be copied; 
         * since HTML is totally replaced, event listeners are wired to the new HTML. */
        cloned._domNode.innerHTML = this.domNode.innerHTML;
    }

    _cloneDescendents(cloned) /* verified */ {
        /*  If children, the constructor's default stencil-built 
         * inner HTML and event listeners should be left in place, 
         * and descendants should be added by cloning recursively. */

        for (let child of this.children) {
            let clonedChild = child.clone();
            cloned.addChild(clonedChild);
        }
    }

    // endregion tree

    // region destructor() and dependencies

    /* Called by a parent's subtractChild(). */
    destructor() /* passed */ {
        /* Post-order clean-up. */

        this._destructDescendants();
        this._destructSelf();
    }

    _destructDescendants() /* verified */ {
        // Use of local array enables safe 
        // traversal while destructing.
        let children = [ ...this._children ];

        // Actually destructing.
        for (let child of children) {
            child.destructor();
        }

        // Trimming the Composite.
        this._children = [];
    }

    _destructSelf() /* verified */ {
        this.parent = null;
        this.isDisplayed = false;

        this._unwireSelfEventListeners();
        this._removeSelfFromStorage();
    }

    _unwireSelfEventListeners() /* passed */ {
        // The event listeners originally defined, 
        // retained, then added are now removed.
        for (let key of this._eventListeners.keys()) {
            let value = this._eventListeners.get(key);
            this.domNode.removeEventListener(key, value);
        }
    }

    _removeSelfFromStorage() {
    }

    // endregion destructor() and dependencies

}
