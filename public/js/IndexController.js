/**/

import { Blurb } from "./Blurb.js";
import { BlurbSet } from "./BlurbSet.js";
import { Storage } from "./Storage.js";
import { Dom } from "./Dom.js";
import { IO } from "./IO.js";

export class IndexController {
    constructor(document) {
        Dom.singleton = new Dom(document);

        this.dom = Dom.singleton;
        this.blurbSets = [];
        this.storage = new Storage();
        this.io = new IO();

        // Used to give all blurb-set and blurb subtrees individual
        // IDs so they can be dragged and dropped accurately.
        this.idIndex = 0;
    }

    init() {
        this.retrieveAnyBlurbContent();
        this.displayAllBlurbContent();
    }
    
    retrieveAnyBlurbContent() {
        // Getting blurb sets in the order last displayed, 
        // or coercing an empty list if never saved yet.
        let setNamesAsJson = this.storage.getItem("BlurbSetNames");
        setNamesAsJson = setNamesAsJson || "[]";

        let setNames = JSON.parse(setNamesAsJson);

        // Retrieving in order for later displaying in that order.
        for (let setName of setNames) {
            let asJson = this.storage.getItem(setName);
            let blurbSet = BlurbSet.fromJson(asJson);
            this.blurbSets.push(blurbSet);
        }
    }


    // region Displaying contents

    displayAllBlurbContent() {
        // Blurb sets and their blurbs, if focused.
        for (let blurbSet of this.blurbSets) {
            this.displayBlurbSet(blurbSet);

            if (blurbSet.isFocused) {
                let region = this.displayBlurbSetRegion(blurbSet);
                this.displayBlurbs(blurbSet.blurbs, region);
                this.displayEmptyBlurb(region);
            }
        }

        this.displayEmptyBlurbSet();
    }

    redisplayAllBlurbContent() {
        // Clearing all prior content.
        this.clearAllChildrenOf(this.dom.blurbSets);
        this.clearAllChildrenOf(this.dom.blurbs);

        // Displaying again.
        this.displayAllBlurbContent();
    }

    clearAllChildrenOf(el) {
        while (el.children.length > 0) {
            el.removeChild(el.children[0]);
        }
    }

    displayBlurbSet(blurbSet) {
        this.displayNode(
            this.dom.groupElStencil,
            blurbSet,
            "name",
            "Name",
            this.dom.blurbSets,
            this.addBlurbSetEventListeners
        );
    }

    displayEmptyBlurbSet() {
        let empty = new BlurbSet(null, []);
        this.displayNode(
            this.dom.emptyBlurbGroupElStencil,
            empty,
            "name",
            "Name",
            this.dom.blurbSets,
            this.addEmptyBlurbSetEventListeners
        );
    }

    displayBlurbSetRegion(blurbSet) {
        // New region.
        let region = this.dom.groupBlurbsElStencil.cloneNode(true);
        region.id = this.idIndex++;

        // And its name.
        let name = region.querySelector("#Name");
        name.innerText = blurbSet.name;

        // Now onto the R blurbs pane.
        this.dom.blurbs.appendChild(region);

        // So caller can add children to the region.
        return region;
    }

    displayBlurbs(blurbs, region) {
        for (let blurb of blurbs) {
            this.displayBlurb(blurb, region);
        }
    }

    displayBlurb(blurb, region) {
        let node = this.displayNode(
            this.dom.blurbElStencil,
            blurb,
            "text",
            "Text",
            region,
            this.addBlurbEventListeners
        );

        let content = node.querySelector("#BlurbContent");
        content.classList.add(blurb.level);
    }

    displayEmptyBlurb(region) {
        let empty = new Blurb(null, "normal");

        this.displayNode(
            this.dom.emptyBlurbElStencil,
            empty,
            "text",
            "Text",
            region,
            this.addEmptyBlurbEventListeners
        );
    }

    displayNode(stencil, source, textSource, textDisplay, group, wirer) {
        let node = stencil.cloneNode(true);
        node.id = this.idIndex++;

        // The display of the text can vary.
        let textDisplayId = `#${ textDisplay }`;
        let text = node.querySelector(textDisplayId);

        // The source of the text can vary.
        text.value = source[textSource];

        wirer(node, this);

        group.appendChild(node);

        // For callers to modify the new node.
        return node;
    }

    // endregion Displaying contents


    // region Adding event listeners

    addBlurbSetEventListeners(node, self) {
        /* Instance passed as "self" since this is called
           using delegation syntax and :. is denatured. */

        node.addEventListener("dragstart", (e) => {
            self.atDragStart(e);
        });
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbSetDrop(e);
        });

        let dumpButton = node.querySelector("#DumpSetButton");

        dumpButton.addEventListener("click", (e) => {
            self.atDumpSetButtonClick(e);
        });
    }

    addBlurbEventListeners(node, self) {
        /* Instance passed as "self" since this is called
           using delegation syntax and :. is denatured. */

        node.addEventListener("dragstart", (e) => {
            self.atDragStart(e);
        });
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });

        node.addEventListener("drop", (e) => {
            self.atBlurbDrop(e);
        });
        node.addEventListener("click", (e) => {
            self.atBlurbClick(e);
        });

        let dumpButton = node.querySelector("#DumpBlurbButton");
        dumpButton.addEventListener("click", (e) => {
            self.atDumpBlurbButtonClick(e);
        });

        let useButton = node.querySelector("#UseBlurbButton");
        useButton.addEventListener("click", (e) => {
            self.atCopyButtonClick(e);
        });
    }

    addEmptyBlurbSetEventListeners(node, self) {
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbSetDrop(e);
        });

        let addButton = node.querySelector("#AddBlurbSetButton");
        addButton.addEventListener("click", (e) => {
            self.addNewBlurbSet(e);
        });
    }

    addEmptyBlurbEventListeners(node, self) {
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbDrop(e);
        });

        let addButton = node.querySelector("#AddBlurbButton");
        addButton.addEventListener("click", (e) => {
            self.addNewBlurb(e);
        });
    }

    // endregion Adding event listeners


    // region Drag and drop eventing

    atDragStart(e) {
        let id = e.target.id;
        e.dataTransfer.setData("name/plain", id);
        e.dataTransfer.dropEffect = "move";
    }

    atDragOver(e) {
        e.preventDefault();
        let over = e.currentTarget;
        over.classList.add("over");
    }

    atDragLeave(e) {
        e.preventDefault();
        let over = e.currentTarget;
        over.classList.remove("over");
    }

    atBlurbSetDrop(e) {
        e.preventDefault();

        // Identifying draggee and droppee.
        let id = e.dataTransfer.getData("name/plain");
        let dragged = this.dom.doc.getElementById(id);

        let onto = e.currentTarget;
        onto.classList.remove("over");

        // Moving the item onscreen.
        this.dom.blurbSets.removeChild(dragged);
        this.dom.blurbSets.insertBefore(dragged, onto);

        // Splicing out from the drag point.
        let source = this.blurbSets;

        // Getting identifiers to find splice indices.
        let fromText = this.textFromEl(dragged, "Name");
        let toText = this.textFromEl(onto, "Name");

        // Getting the moved blurb set itself.
        let draggedBlurbSet = this.setFromSetName(fromText);

        // Splicing out.
        let from = source.findIndex((x) => x.name === fromText);
        source.splice(from, 1);

        // Splicing in, after splice-out so indices are right.
        let to = source.findIndex((x) => x.name === toText);
        to = to !== -1 ? to : source.length;

        source.splice(to, 0, draggedBlurbSet);

        // Updating the stored order for the future.
        this.updateBlurbSetNames();

        // Changing the visible order of the blurb regions.
        this.redisplayAllBlurbContent();
    }

    atBlurbDrop(e) {
        e.preventDefault();

        // Identifying draggee and droppee.
        let id = e.dataTransfer.getData("name/plain");
        let dragged = this.dom.doc.getElementById(id);

        let onto = e.currentTarget;
        onto.classList.remove("over");

        // Getting parents, for visible dnd and internal changing.
        let parentFrom = dragged.parentNode;
        let parentOnto = onto.parentNode;

        // Moving the item onscreen.
        parentFrom.removeChild(dragged);
        parentOnto.insertBefore(dragged, onto);

        // Getting names of blurb sets to then find them.
        let fromName = this.setNameFromBlurbParent(parentFrom);
        let toName = this.setNameFromBlurbParent(parentOnto);

        // Getting the blurb sets, then their blurbs.
        let from = this.setFromSetName(fromName);
        let to = this.setFromSetName(toName);

        // Getting identifiers to find splice indices.
        let fromText = this.textFromEl(dragged, "Text");
        let toText = this.textFromEl(onto, "Text");

        // Getting the actual blurbs to work with.
        let draggedBlurb = from.blurbFromText(fromText);
        let ontoBlurb = to.blurbFromText(toText);

        // Splicing out from the drag point.
        from.dumpBlurb(draggedBlurb);

        // Splicing into the drop point.
        // Has to be calculated after splice-out,
        // or the index for splice-in may be wrong.
        to.addBlurb(draggedBlurb, ontoBlurb);

        // Saving changes to storage.
        from.save();
        to.save();
    }

    // endregion Drag and drop eventing


    // region Copy eventing and dependencies

    atBlurbClick(e) {
        let target = e.target;

        // If a child el with another purpose was clicked, ignore this click.
        if (!this.elHasAnyOfTheseClasses(target, "clickable", "inner-blurb")) {
            e.stopPropagation();
            return;
        }

        this.copyFromBlurbText(target);
    }

    atCopyButtonClick(e) {
        let target = e.target;
        this.copyFromBlurbText(target);
    }

    copyFromBlurbText(el) {
        let blurbEl = this.blurbElFromEl(el);
        let text = this.textFromEl(blurbEl, "Text");

        this.io.writeToClipboard(text)
            .then(() => {
                    this.flashBlurbEl(blurbEl);
                }
            );
    }

    flashBlurbEl(blurbEl) {
        let target = blurbEl.querySelector(".inner-blurb");
        let text = target.querySelector("#Text");
        target.classList.add("flash");
        text.classList.add("flash");
        setTimeout(() => {
                target.classList.remove("flash");
                text.classList.remove("flash");
            }, 250
        );
    }

    // endregion Copy eventing and dependencies


    // region Add eventing

    addNewBlurbSet(e) {
        // Getting blurb set contents.
        let target = e.target;
        let setEl = this.ancestorFromElAndClass(target, "blurb-set");
        let text = this.textFromEl(setEl, "Name");

        // Actually adding new, empty blurb set.
        let blurbSet = new BlurbSet(text, []);
        this.blurbSets.push(blurbSet);

        // Saving the empty blurb set to storage in both ways.
        blurbSet.save();
        this.updateBlurbSetNames();

        // Redisplaying everything, with the new blurb set included.
        this.redisplayAllBlurbContent();
    }

    addNewBlurb(e) {
        // Getting blurb contents.
        let target = e.target;
        let blurbEl = this.ancestorFromElAndClass(target, "inner-blurb");
        let text = this.textFromEl(blurbEl, "Text");

        // Getting blurb set.
        let region = this.ancestorFromElAndClass(target, "region");
        let blurbSet = this.setFromBlurbParent(region);

        // Actually adding blurb.
        let blurb = new Blurb(text, "plain");
        blurbSet.blurbs.push(blurb);

        // Saving the blurb to storage.
        blurbSet.save();

        // Removing just the set's blurbs.
        let blurbNodes = region.querySelectorAll(".blurb");

        for (let blurbNode of blurbNodes) {
            region.removeChild(blurbNode);
        }

        // Redrawing just the set's blurbs.
        this.displayBlurbs(blurbSet.blurbs, region);
        this.displayEmptyBlurb(region);
    }

    // endregion Add eventing


    // region Delete eventing

    atDumpSetButtonClick(e) {
        // Identifying blurb set.
        let target = e.target;
        let setEl = this.ancestorFromElAndClass(target, "blurb-set");
        let setName = this.textFromEl(setEl, "Name");

        let source = this.blurbSets;

        // Finding the blurb set and its index in the array.
        let from = source.findIndex((x) => x.name === setName);
        let blurbSet = source[from];

        // Dumping the blurb set from array.
        source.splice(from, 1);

        // Removing the blurb set from the UI.
        this.dom.blurbSets.removeChild(setEl);

        // Removing the blurb set's pane from the UI.
        let regions = this.dom.blurbs
            .querySelectorAll(".region");

        for (let region of regions) {
            let regionName = this.nameFromEl(region);

            if (regionName === setName) {
                this.dom.blurbs.removeChild(region);
                break;
            }
        }

        // Removing the blurb set from local storage both ways.
        blurbSet.dump();
        this.updateBlurbSetNames();
    }

    atDumpBlurbButtonClick(e) {
        let target = e.target;

        let blurbEl = this.ancestorFromElAndClass(target, "blurb");
        let region = this.ancestorFromElAndClass(blurbEl, "region");

        // Drop from the blurb set.
        let blurbSet = this.setFromBlurbParent(region);
        let text = this.textFromEl(blurbEl, "Text");
        let blurb = blurbSet.blurbFromText(text);
        blurbSet.dumpBlurb(blurb);

        // Drop from the stored blurb set.
        blurbSet.save();

        // Drop from the UI.
        region.removeChild(blurbEl);
    }

    // endregion Delete eventing


    // region DOM and object traversal

    blurbElFromEl(el) {
        let blurbEl = this.ancestorFromElAndClass(el, "blurb");
        return blurbEl;
    }

    setFromBlurbChildEl(el) {
        let blurbParent = this.blurbElFromEl(el);
        let setName = this.setNameFromBlurbParent(blurbParent);
        let set = this.setFromSetName(setName);
        return set;
    }

    setFromBlurbParent(parent) {
        let name = this.setNameFromBlurbParent(parent);
        let set = this.setFromSetName(name);
        return set;
    }

    setNameFromBlurbParent(parent) {
        let nameEl = parent.querySelector("#Name");
        let name = nameEl.innerText;
        return name;
    }

    setFromSetName(name) {
        let set = this.blurbSets
            .find((x) => x.name === name);
        return set;
    }

    textFromEl(rootEl, textElName) {
        /* The textEl here is assumed to be an Input
           or TextArea with a .value attribute. */

        textElName = `#${ textElName }`;
        let textEl = rootEl.querySelector(textElName);

        let text = textEl.value;
        return text;
    }

    nameFromEl(el) {
        /* The nameEl here is assumed to be a Div or other
           non-input node with an .innerText attribute. */

        let nameEl = el.querySelector("#Name");
        let name = nameEl.innerText;
        return name;
    }

    ancestorFromElAndClass(child, name) {
        let node = child.parentNode;

        while (!node.classList.contains(name)) {
            node = node.parentNode;
        }

        return node;
    }

    elHasAnyOfTheseClasses(el, ...names) {
        for (let name of names) {
            if (el.classList.contains(name)) {
                return true;
            }
        }

        return false;
    }

    // endregion DOM and object traversal


    // region Blurb set order

    updateBlurbSetNames() {
        let blurbSetNames = this.blurbSets.map(x => x.name);
        let asJson = JSON.stringify(blurbSetNames);
        this.storage.setItem("BlurbSetNames", asJson);
    }

    // endregion Blurb set order


    saveAllBlurbSets() {
        // Saving all current blurb sets.
        for (let blurbSet of this.blurbSets) {
            blurbSet.save();
        }
    }
}
