/**/

import { DomEl } from "./DomEl.js";

/* The empty equivalent of a GroupEl, which displays 
   the name of a group and its group buttons. */
export class EmptyGroupEl extends DomEl {
    /* &cruft, aside from displaying itself, this class 
       may have options related to initing a new group */

    /* An override. */
    get class() {
        return "EmptyGroupEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.emptyGroupElStencil;
    }
    
    get domParent() {
        return this._domParent;
    }

    constructor(domParent) {
        /* An EmptyGroupEl doesn't have the usual 
           fork parent, nor any child nodes. */

        super();
        this._domParent = domParent;
    }
    
    _displaySelf() {
        this._domParent.appendChild(this.domNode);
    }
}
