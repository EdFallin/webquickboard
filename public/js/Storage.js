/**/

export class Storage {
    constructor() {
        this.length = localStorage.length;
    }
    
    key(at) {
        return localStorage.key(at);
    }
    
    getItem(key) {
        return localStorage.getItem(key);
    }
    
    setItem(key, value) {
        return localStorage.setItem(key, value);
    }
    
    removeItem(key) {
        return localStorage.removeItem(key);
    }
}
