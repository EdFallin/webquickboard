/**/

import { DomEl } from "./DomEl.js";

export class EmptyBlurbEl extends DomEl {
    /* &cruft, aside from displaying itself, this class 
       may have options related to initing a new blurb */

    /* An override. */
    get class() {
        return "EmptyBlurbEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.emptyBlurbElStencil;
    }
    
    get domParent() {
        return this._domParent;
    }
    
    constructor(domParent) {
        /* An EmptyBlurbEl doesn't have the usual 
           fork parent, nor any child nodes. */

        super();
        this._domParent = domParent;
    }
    
    _displaySelf() {
        this._domParent.appendChild(this.domNode);
    }
}
