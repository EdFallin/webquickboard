/**/

export class Dom {
    constructor(doc) /* passed */ {
        this.doc = doc;

        /* Stencils retained in the DOM for cloning to create new display elements. */
        this.groupElStencil = doc.getElementById("GroupElStencil");
        this.groupBlurbsElStencil = doc.getElementById("GroupBlurbsElStencil");
        this.blurbElStencil = doc.getElementById("BlurbElStencil");

        this.emptyBlurbGroupElStencil = doc.getElementById("EmptyBlurbGroupElStencil");
        this.emptyBlurbElStencil = doc.getElementById("EmptyBlurbElStencil");

        /* DOM nodes corresponding to the roots of the two DomEl Composites. */
        this.blurbSets = doc.getElementById("BlurbSets");
        this.blurbs = doc.getElementById("Blurbs");

        /* Basis for IDs that are unique across all DomEl subclass objects, 
           1-based because incremented before supplied to consumer. */
        this._id = 0;
    }

    static get singleton() /* passed */ {
        Dom._singleton = Dom._singleton || new Dom(document);
        return Dom._singleton
    }

    static set singleton(value) /* passed */ {
        Dom._singleton = value;
    }

    get nextId() /* passed */ {
        return ++this._id;
    }
}
