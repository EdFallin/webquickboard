/**/

import { DomEl } from "./DomEl.js";
import { GroupBlurbsEl } from "./GroupBlurbsEl.js";
import { GroupsRootEl } from "./GroupsRootEl.js";

/* Displays the name of a blurb group and its buttons.  Exposes methods to add 
   and drop itself and to display the blurbs via the BlurbDisplay root node. */
export class GroupEl extends DomEl {
    // region properties

    /* An override. */
    get class() {
        return "GroupEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.groupElStencil;
    }

    get name() {
        return this._name;
    }

    get blurbSet() {
        return this._blurbSet;
    }

    get blurbs() {
        return this._blurbs;
    }

    get displayer() {
        return this._displayer;
    }

    // endregion properties

    // region constructor() and dependencies

    /* The .displayer is a GroupBlurbsEl, found in the R-side Composite, 
       responding to events and displaying the group's blurbs when visible. */
    constructor(blurbSet, parent) {
        super(parent);

        this._blurbSet = blurbSet;
        this._name = blurbSet.name;
        this._blurbs = blurbSet.blurbs;

        this._initDisplayer();
    }

    _initDisplayer() {
        this._displayer = new GroupBlurbsEl(this._blurbSet, this);
    }

    // endregion constructor() and dependencies

    // region displaying

    _displaySelf() {
        this._displaySelfOnListRoot();
        this._displayName();
        this._displayGroupBlurbs();
    }

    _displaySelfOnListRoot() {
        let at = this.parent.children.indexOf(this);

        let listRoot = this.parent.listRoot;
        let before = listRoot.children[at];

        listRoot.insertBefore(this.domNode, before);
    }

    _displayName() {
        let nameNode = this._domNode.querySelector("#Name");
        nameNode.value = this._name;
    }

    _displayGroupBlurbs() {
        if (this._blurbSet.isFocused) {
            this._displayer.display();
        }
    }

    // endregion displaying

    canDropThisOnto(domEl) /* passed */ {
        /* These return values mean that GroupEls can 
           be dropped only within the L Composite. */
    
        if (domEl instanceof GroupEl) {
            return true;
        }

        if (domEl instanceof GroupsRootEl) {
            return true;
        }

        return false;
    }

    _whenDroppedOntoThis(e) {
        e.stopPropagation();
        let dragged = this._addToParent(e);

        // Displays the GroupEl, and its 
        // GBE if it should be displayed.
        dragged.display();
    }

    /* An override. */
    clone() {
        let cloned = new this.constructor(this.blurbSet, this.parent);

        /* No child objects. */
        this._cloneLeafHtml(cloned);

        return cloned;
    }

    destructor() {
        this._displayer.destructor();
        super.destructor();
    }

    /* &cruft, work into object system */
    atDumpSetButtonClick(e) {
        // Identifying blurb set.
        let target = e.target;
        let setEl = this.ancestorFromElAndClass(target, "blurb-set");
        let setName = this.textFromEl(setEl, "Name");

        let source = this.blurbSets;

        // Finding the blurb set and its index in the array.
        let from = source.findIndex((x) => x.name === setName);
        let blurbSet = source[from];

        // Dumping the blurb set from array.
        source.splice(from, 1);

        // Removing the blurb set from the tree and DOM.
        NewDom.singleton.blurbSets.removeChild(setEl);
        setEl.domNode.remove();

        // Removing the blurb set's pane from the tree and DOM.
        let regions = NewDom.singleton.blurbs
            .querySelectorAll(".region");

        for (let region of regions) {
            let regionName = this.nameFromEl(region);

            if (regionName === setName) {
                NewDom.singleton.blurbs.removeChild(region);
                region.domNode.remove();
                break;
            }
        }

        // Removing the blurb set from local storage both ways.
        blurbSet.dump();
        this.updateBlurbSetNames();
    }

    expand() {
        /* &cruft, needs to be implemented in GroupBlurbsEl and/or here */

        this._displayGroupBlurbs();
    }

    collapse() {
        /* &cruft, needs to be implemented in GroupBlurbsEl and/or here */

        /* Destroying the displayer also removes its content from the DOM. */
        this._displayer.destructor();
        this._displayer = null;
    }

}
