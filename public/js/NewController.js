/**/

import { Blurb } from "./Blurb.js";
import { BlurbSet } from "./BlurbSet.js";
import { Storage } from "./Storage.js";
import { NewDom } from "./NewDom.js";
import { IO } from "./IO.js";

// Display of blurbs and groups.
import { BlurbEl } from "./BlurbEl.js";
import { GroupEl } from "./GroupEl.js";
import { GroupBlurbsEl } from "./GroupBlurbsEl.js";

// Input of blurbs and groups.
import { EmptyBlurbEl } from "./EmptyBlurbEl.js";
import { EmptyGroupEl } from "./EmptyGroupEl.js";

// Roots of each display / content Composite.
import { GroupsRootEl } from "./GroupsRootEl.js";
import { BlurbsRootEl } from "./BlurbsRootEl.js";

export class NewController {
    constructor(document) {
        NewDom.singleton = new NewDom(document);

        this.dom = NewDom.singleton;
        this.blurbSets = [];
        this.storage = new Storage();
        this.io = new IO();
        
        /* &cruft, remove, and maybe remove all IDs? */
        // Used to give all blurb-set and blurb subtrees individual
        // IDs so they can be dragged and dropped accurately.
        this.idIndex = 0;
    }

    init() {
        this.retrieveAnyBlurbContent();
        this.displayAllBlurbContent();
    }

    retrieveAnyBlurbContent() {
        // Getting blurb sets in the order last displayed, 
        // or coercing an empty list if never saved yet.
        let setNamesAsJson = this.storage.getItem("BlurbSetNames");
        setNamesAsJson = setNamesAsJson || "[]";

        let setNames = JSON.parse(setNamesAsJson);

        // Retrieving in order for later displaying in that order.
        for (let setName of setNames) {
            let asJson = this.storage.getItem(setName);
            let blurbSet = BlurbSet.fromJson(asJson);
            this.blurbSets.push(blurbSet);
        }
    }

    displayAllBlurbContent() {
        // The R-side Composite, empty until filled by L side.
        this.blurbsRoot = new BlurbsRootEl(null);
        GroupBlurbsEl.root = this.blurbsRoot;
        
        // Displaying the R Composite's root.
        // Children done later, cross-Composite.
        this.blurbsRoot.display();

        // From data to objects for L-side Composite, 
        // and ultimately for R-side Composite as well.
        let blurbGroupsEls = [];

        for (let blurbSet of this.blurbSets) {
            let blurbGroup = new GroupEl(blurbSet, null);
            blurbGroupsEls.push(blurbGroup);
        }

        // Assembling the Composites.
        this.groupsRoot = new GroupsRootEl(null, blurbGroupsEls);

        // Recursive action on L Composite from root.
        this.groupsRoot.display();
    }

    displayEmptyBlurbSet() {
        let empty = new BlurbSet(null, []);
        this.displayNode(
            this.dom.emptyBlurbGroupElStencil,
            empty,
            "name",
            "Name",
            this.dom.blurbSets,
            this.addEmptyBlurbSetEventListeners
        );
    }

    displayEmptyBlurb(region) {
        let empty = new Blurb(null, "normal");

        this.displayNode(
            this.dom.emptyBlurbElStencil,
            empty,
            "text",
            "Text",
            region,
            this.addEmptyBlurbEventListeners
        );
    }

    updateBlurbSetNames() {
        let blurbSetNames = this.blurbSets.map(x => x.name);
        let asJson = JSON.stringify(blurbSetNames);
        this.storage.setItem("BlurbSetNames", asJson);
    }

    saveAllBlurbSets() {
        // Saving all current blurb sets.
        for (let blurbSet of this.blurbSets) {
            blurbSet.save();
        }
    }
}
