/**/

// Encapsulates some name to be displayed and copied,
// along with its semantic color (level) and its 
// sizing for immediate readability after rendering.
// DOM indices are not retained, since they would  
// not make sense over time with many set changes.
export class Blurb {
    // region Levels

    static h1 = "h1";
    static h2 = "h2";
    static h3 = "h3";
    static good = "good";
    static bad = "bad";
    static neutral = "neutral";
    static plain = "plain";

    // endregion Levels

    constructor(text, level, height, width, index) {
        // Content.
        this.text = text;
        this.level = level;

        // Appearance.
        this.height = height;
        this.width = width;

        // Identity.
        this.index = index;
    }

    static fromJson(json) {
        // Output object.
        let blurb = new Blurb();

        // Deserialize.
        let asJson = JSON.parse(json);

        // Copy to output. 
        blurb.text = asJson.name ?? "";
        blurb.level = asJson.level ?? Blurb.plain;
        blurb.height = asJson.height ?? 0;
        blurb.width = asJson.width ?? 0;

        // Out to caller.
        return blurb;
    }

}
