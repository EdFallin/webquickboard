/**/

/* A Singleton that retains cross-object drag-and-drop state. */
export class DragAndDrop {
    static get singleton() /* verified */ {
        // Lazy initing, if .singleton wasn't set at the start.
        if (!DragAndDrop._singleton) {
            DragAndDrop._singleton = new DragAndDrop();
        }

        return DragAndDrop._singleton;
    }

    static set singleton(instance) /* verified */ {
        DragAndDrop._singleton = instance;
    }

    constructor() {
        this._dragged = null;
    }

    get dragged() /* verified */ {
        return this._dragged;
    }

    set dragged(item) /* verified */ {
        this._dragged = item;
    }
    
}
