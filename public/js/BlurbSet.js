/**/

import { Storage } from "./Storage.js";

export class BlurbSet {
    constructor(name, blurbs, index, isFocused) {
        this.name = name;
        this.blurbs = blurbs;
        this.index = index || 0;
        this.isFocused = isFocused || true;

        this.storage = new Storage();
    }

    static fromJson(json) /* passed */ {
        let plain = JSON.parse(json);
        let set = new BlurbSet(plain.name, plain.blurbs);

        if (plain.index !== undefined) {
            set.index = plain.index;
        }

        if (plain.isFocused !== undefined) {
            set.isFocused = plain.isFocused;
        }

        return set;
    }

    blurbFromText(text) {
        let blurb = this.blurbs.find((x) => x.text === text);
        return blurb;
    }

    addBlurb(blurb, at) {
        // Default if no "at", for instance when adding at the end.
        let to = this.blurbs.length;

        // Common case: there is an "at".
        if (at !== undefined) {
            to = this.blurbs.findIndex((x) => x.text === at.text);
        }

        // Edge case: "at" was not found.
        if (to === -1) {
            to = this.blurbs.length;
        }

        // Adding where dropped.
        this.blurbs.splice(to, 0, blurb);
    }

    dumpBlurb(blurb) {
        let from = this.blurbs.findIndex((x) => x.text === blurb.text);
        this.blurbs.splice(from, 1);
    }

    save() {
        let asJson = JSON.stringify(this);
        this.storage.setItem(this.name, asJson);
    }

    dump() {
        this.storage.removeItem(this.name);
    }
}
