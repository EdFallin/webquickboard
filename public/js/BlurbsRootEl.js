/**/

import { DomEl } from "./DomEl.js";

/* A root node for display of the blurbs themselves.  Its child 
   nodes are GroupBlurbsEls, whose child nodes are BlurbEls. */
export class BlurbsRootEl extends DomEl {
    /* Probably not a lot of special actions here. */

    /* An override. */
    get class() {
        return "BlurbsRootEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.blurbsRoot;
    }

    /* The .children here are GroupBlurbsEl objects. */
    constructor(parent, children) {
        super(parent, children);
    }

    _initDomNode() {
        this._domNode = this.stencil;
    }
    
    _displaySelf() {
        /* No operations.  BlurbsRootEl is the root of its Composite, 
           with no .parent, while its .domNode is always displayed. */
    }

}
