/**/

import { DomEl } from "./DomEl.js";
import { BlurbEl } from "./BlurbEl.js";
import { EmptyBlurbEl } from "./EmptyBlurbEl.js";

/* Displays just one group of blurbs.  
   Its child nodes are BlurbEls.
   A child node of BlurbsRootEl. */
export class GroupBlurbsEl extends DomEl {
    // region static properties

    /* The root is the same for all GroupBlurbsEls, so it 
       can most easily be set once as a static property. */

    static get root() {
        return GroupBlurbsEl._root;
    }

    static set root(item) {
        GroupBlurbsEl._root = item;
    }

    // endregion static properties

    // region instance properties

    /* An override. */
    get class() {
        return "GroupBlurbsEl";
    }

    /* An override. */
    get stencil() {
        return this._dom.groupBlurbsElStencil;
    }

    get name() {
        return this._name;
    }

    get listRoot() {
        return this._listRoot;
    }

    get blurbSet() {
        return this._blurbSet;
    }

    get emptyBlurb() {
        return this._emptyBlurb;
    }

    get groupEl() {
        return this._groupEl;
    }

    // endregion instance properties

    // region constructor() and dependencies

    constructor(blurbSet, groupEl) {
        // All group blurbs have the same parent.
        let parent = GroupBlurbsEl.root;

        // Children are generated from stored content.
        let children = blurbSet.blurbs
            .map(x => new BlurbEl(x, null));

        // Most members, defined by DomEl.
        super(null, children);

        // Reference back to cross-Composite object.
        this._groupEl = groupEl;

        // Additional members.
        this.parent = parent;
        this._blurbSet = blurbSet;
        this._name = this._blurbSet.name;

        this._initEmptyBlurb();
    }
    
    _initEmptyBlurb() {
        // A GroupBlurbsEl has a final empty 
        // blurb for direct blurb entry.
        let empty = this._domNode.querySelector("#Empty");
        this._emptyBlurb = new EmptyBlurbEl(empty);
    }
    
    // endregion constructor() and dependencies

    _displaySelf() {
        this._addSelf();

        super._displaySelf();
        this._setListRoot();
        this._setNodeName();

        this._displayEmptyBlurb();
    }

    _addSelf() {
        // Relative index from GE, and after (or undefined) for inserting.
        let groupEl = this._groupEl;
        let at = groupEl.parent.children.indexOf(groupEl);

        // Localizing because `this.parent` 
        // is null after removeChild().
        let parent = this.parent;

        // If self is child of .parent already, 
        // it should be removed before re-adding.
        if (parent.children.includes(this)) {
            parent.removeChild(this);
            this.domNode.remove();
        }

        // Position after removing is 
        // the right one for inserting.
        let before = parent.children[at];

        // Actually adding.
        parent.addChild(this, before);
    }

    _setListRoot() {
        /* Local-subtree DOM node with ListRoot as ID. */

        this._listRoot = this._domNode.querySelector("#ListRoot");
    }

    _setNodeName() {
        let name = this._domNode.querySelector("#Name");
        name.innerText = this._name;
    }

    _displayEmptyBlurb() {
        this._emptyBlurb.display();
    }

    _whenDroppedOntoThis(e) {
        let dragged = this._addAsChild(e);
        dragged.display();
    }

    /* An override. */
    _calculateOntoChild(e) {
        /* This algorithm is the same as the one in DomEl, except that it 
           looks at .listRoot instead of .domNode to determine drop point, 
           since .children correspond to the DOM children of .listRoot. */

        // Default used if drop is after last child.
        let onto = null;

        // Finding any first child that the drop is above, 
        // using a numeric loop for onscreen DOM index.
        for (let at = 0; at < this.listRoot.children.length; at++) {
            let domChild = this.listRoot.children[at];

            let box = domChild.getBoundingClientRect();
            let top = box.top;

            if (e.clientY < top) {
                onto = this.children[at];
                break;
            }
        }

        return onto;
    }
}
